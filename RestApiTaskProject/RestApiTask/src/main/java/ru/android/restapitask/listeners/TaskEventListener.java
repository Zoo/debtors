package ru.android.restapitask.listeners;

import android.database.Cursor;
import android.os.Bundle;

/**
 * Created by dev4 on 25.02.14.
 */
public interface TaskEventListener {
    public void fireTaskEvent(Bundle result,int code);
    public void returnDataFromTable(Cursor cursor);
}
