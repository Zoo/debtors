package ru.android.restapitask.tasks;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.fragments.MainFragment;
import ru.android.restapitask.listeners.TaskEventListener;

/**
 * Created by dev4 on 27.02.14.
 */
public class CheckIsDBEmptyTask extends AsyncTask<Void,Void,Boolean> {

    TaskEventListener taskEventListener;

    public CheckIsDBEmptyTask(TaskEventListener taskEventListener) {
        this.taskEventListener = taskEventListener;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean exist = null;
        Cursor cursor = DBPhotos.getInstance().getAllDataFromTable(DBPhotos.TABLE_PHOTOS);
        if (cursor != null){
            exist = cursor.moveToFirst();
            cursor.close();
        }
        return exist;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        Bundle bundle = new Bundle();
        bundle.putBoolean(MainFragment.BUNDLE_PARAM_DATA,result);
        taskEventListener.fireTaskEvent(bundle,MainFragment.FIRE_TASK_EVENT_ACTION);
    }
}
