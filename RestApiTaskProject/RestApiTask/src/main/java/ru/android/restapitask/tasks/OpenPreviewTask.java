package ru.android.restapitask.tasks;

import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.listeners.FragmentEventListener;
import ru.android.restapitask.fragments.MainFragment;
import ru.android.restapitask.fragments.PhotoPreviewFragment;

/**
 * Created by dev4 on 26.02.14.
 */
public class OpenPreviewTask extends AsyncTask<Bundle, Void, Void> {

    FragmentEventListener fragmentEventListener;

    public OpenPreviewTask(FragmentEventListener fragmentEventListener) {
        this.fragmentEventListener = fragmentEventListener;
    }

    @Override
    protected Void doInBackground(Bundle... params) {
        String tableName = params[0].getString(MainFragment.BUNDLE_PARAM_TABLE_NAME);
        Long id = params[0].getLong(MainFragment.BUNDLE_PARAM_DATA);

        Cursor cursor = DBPhotos.getInstance().getRecord(tableName, id);
        if (cursor != null){
            try {
                cursor.moveToFirst();
                Bundle fields = new Bundle();
                Uri pathLocal = Uri.parse(cursor.getString(cursor.getColumnIndex(DBPhotos.COLUMN_LOCAL_PATH)));
                fields.putLong(DBPhotos.COLUMN_ID, id);
                fields.putParcelable(DBPhotos.COLUMN_LOCAL_PATH, pathLocal);
                fields.putDouble(DBPhotos.COLUMN_LATITUDE, cursor.getDouble(cursor.getColumnIndex(DBPhotos.COLUMN_LATITUDE)));
                fields.putDouble(DBPhotos.COLUMN_LONGITUDE, cursor.getDouble(cursor.getColumnIndex(DBPhotos.COLUMN_LONGITUDE)));
                fields.putInt(DBPhotos.COLUMN_IS_UPLOADED, cursor.getInt(cursor.getColumnIndex(DBPhotos.COLUMN_IS_UPLOADED)));
                fields.putString(DBPhotos.COLUMN_PATH_ON_PARSE, cursor.getString(cursor.getColumnIndex(DBPhotos.COLUMN_PATH_ON_PARSE)));
                PhotoPreviewFragment fragment = new PhotoPreviewFragment(fields);
                fragmentEventListener.fragmentEvent(fragment);
            } finally {
                cursor.close();
            }
        }
        return null;
    }
}
