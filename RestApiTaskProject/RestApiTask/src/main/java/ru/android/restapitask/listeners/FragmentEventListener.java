package ru.android.restapitask.listeners;

import android.support.v4.app.Fragment;

/**
 * Created by dev4 on 21.02.14.
 */
public interface FragmentEventListener {
    public void fragmentEvent(Fragment fragment);
}
