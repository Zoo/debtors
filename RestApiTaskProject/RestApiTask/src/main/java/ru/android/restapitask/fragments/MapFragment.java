package ru.android.restapitask.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.R;
import ru.android.restapitask.listeners.FragmentEventListener;
import ru.android.restapitask.listeners.TaskEventListener;
import ru.android.restapitask.tasks.GetDataTask;
import ru.android.restapitask.tasks.OpenPreviewTask;

/**
 * Created by dev4 on 03.03.14.
 */
public class MapFragment extends SupportMapFragment implements TaskEventListener {

    ImageLoader imageLoader;
    GoogleMap map;
    GoogleMap.OnMarkerClickListener onMarkerClickListener;
    FragmentEventListener fragmentEventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()) != 0){
            Log.d("<---Google play service error--->", "Google play service error");
        }
        View view = super.onCreateView(inflater, container, savedInstanceState);
        imageLoader = ImageLoader.getInstance();

        map = getMap();
        initMapMarkers();
        customizeInfoWindow();
        map.setOnMarkerClickListener(onMarkerClickListener);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("Map");

        onMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return false;
            }
        };
        return view;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentEventListener = (FragmentEventListener) getActivity();
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * get all data from table, operations with result
     * are performing in the returnDataFromTable event
     */
    private void initMapMarkers(){
        new GetDataTask(this).execute();
    }

    private void customizeInfoWindow(){
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                long id = Long.parseLong(marker.getSnippet().split("\\|")[1]);
                Bundle bundle =  new Bundle();
                bundle.putString(MainFragment.BUNDLE_PARAM_TABLE_NAME, DBPhotos.TABLE_PHOTOS);
                bundle.putLong(MainFragment.BUNDLE_PARAM_DATA, id);
                new OpenPreviewTask(fragmentEventListener).execute(bundle);
            }
        });

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                ImageView view = new ImageView(getActivity());
                view.setMaxHeight(160);
                view.setMaxWidth(100);
                final Marker mark = marker;
                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .showImageForEmptyUri(R.drawable.ic_launcher)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .cacheOnDisc(true)
                        .cacheInMemory(true)
                        .showImageOnLoading(R.drawable.loading_icon2)
                        .build();
                /**
                 * Snippet containing path to photo on parse.com and id of the photo
                 * devided by "|"
                 */
                String imageUri = marker.getSnippet().split("\\|")[0];
                imageLoader.displayImage(imageUri, view, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        if (mark != null && mark.isInfoWindowShown()) {
                            mark.hideInfoWindow();
                            mark.showInfoWindow();
                        }
                    }
                });
                return view;
            }
        });
    }

    @Override
    public void fireTaskEvent(Bundle result, int code) {

    }

    @Override
    public void returnDataFromTable(Cursor cursor) {
        Double longitude = 0.0;
        Double latitude = 0.0;
        if (cursor != null){
            try{
                if (cursor.moveToFirst()){
                    do {
                        long id = cursor.getLong(cursor.getColumnIndex(DBPhotos.COLUMN_ID));
                        longitude = cursor.getDouble(cursor.getColumnIndex(DBPhotos.COLUMN_LONGITUDE));
                        latitude = cursor.getDouble(cursor.getColumnIndex(DBPhotos.COLUMN_LATITUDE));
                        String path = cursor.getString(cursor.getColumnIndex(DBPhotos.COLUMN_PATH_ON_PARSE));
                        map.addMarker(new MarkerOptions().position(new LatLng(latitude,longitude))).setSnippet(path + "|" + id);
                    }while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
            /**
             * moving camera to the last added marker and zooming
             */
            CameraUpdate position = CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(19);
            map.moveCamera(position);
            map.animateCamera(zoom);
        }
    }
}
