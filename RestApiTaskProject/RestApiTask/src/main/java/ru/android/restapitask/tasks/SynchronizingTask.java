package ru.android.restapitask.tasks;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.activities.MainActivity;
import ru.android.restapitask.fragments.MainFragment;
import ru.android.restapitask.support.Support;

/**
 * Created by dev4 on 27.02.14.
 */
public class SynchronizingTask extends AsyncTask<Void,Void,Void> {

    Context context;
    String urlClasses = "https://api.parse.com/1/classes/MyPhoto";

    public SynchronizingTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {
        synchronizeData();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Toast.makeText(context, "Data synchronized with parse.com", Toast.LENGTH_LONG).show();
    }

    private void synchronizeData(){
        HttpClient client = new DefaultHttpClient();
        HttpResponse httpResponse;
        HttpGet httpGet = new HttpGet(urlClasses);
        httpGet.setHeader(MainActivity.APPLICATION_ID_NAME,MainActivity.APPLICATION_ID);
        httpGet.setHeader(MainActivity.REST_API_KEY_NAME,MainActivity.REST_API_KEY);
        httpGet.setHeader(HTTP.CONTENT_TYPE,"application/json");

        /**
         * Check wifi connection before creating get request
         */
        if (!Support.isConnectedToWIFI(context)){
            return;
        }
        try {
            httpResponse = client.execute(httpGet);

            InputStream is = httpResponse.getEntity().getContent();
            JSONObject jsonObject = parseToJSONObject(is);
            JSONArray results = jsonObject.getJSONArray("results");
            for (int i = 0; i < results.length(); i++){
                JSONObject obj = results.getJSONObject(i);
                ContentValues cv = new ContentValues();
                cv.put(DBPhotos.COLUMN_LOCAL_PATH,obj.getString(DBPhotos.COLUMN_LOCAL_PATH));
                cv.put(DBPhotos.COLUMN_PATH_ON_PARSE,obj.getString(DBPhotos.COLUMN_PATH_ON_PARSE));
                cv.put(DBPhotos.COLUMN_LONGITUDE,obj.getDouble(DBPhotos.COLUMN_LONGITUDE));
                cv.put(DBPhotos.COLUMN_LATITUDE,obj.getDouble(DBPhotos.COLUMN_LATITUDE));
                cv.put(DBPhotos.COLUMN_IS_UPLOADED,obj.getInt(DBPhotos.COLUMN_IS_UPLOADED));
                DBPhotos.getInstance().addRecord(DBPhotos.TABLE_PHOTOS,cv);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private JSONObject parseToJSONObject(InputStream inputStream){
        JSONObject jsonObject = null;
        /**
         * Или можно так, тоже проверил - все работает, но сделал с byte[] для опыта.
         *
         * BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
         * String line;
         * while ((line = reader.readLine()) != null) {
         * builder.append(line);
         * }
         */
        try {
            byte[] temp = new byte[1024];
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int read;
            while ((read = inputStream.read(temp)) != -1){
                bos.write(temp,0,read);
            }
            inputStream.close();
            String JSONString = new String(bos.toByteArray());
            jsonObject = new JSONObject(JSONString);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}