package ru.android.restapitask.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.R;
import ru.android.restapitask.listeners.FragmentEventListener;
import ru.android.restapitask.tasks.OpenPreviewTask;

/**
 * Created by dev4 on 24.02.14.
 */
public class GalleryFragment extends Fragment{

    ImageLoader imageLoader;
    GridView gridView;
    DisplayImageOptions options;
    FragmentEventListener fragmentEventListener;
    Button bBackGallery;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_gallery,null);
        bBackGallery = (Button) view.findViewById(R.id.bBackGallery);

        bBackGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .cacheOnDisc(true)
                .cacheInMemory(true)
                .showImageOnLoading(R.drawable.load)
                .build();

        new getUrlsTask().execute();

        gridView = (GridView) view.findViewById(R.id.gvGallery);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle params = new Bundle();
                params.putString(MainFragment.BUNDLE_PARAM_TABLE_NAME,DBPhotos.TABLE_PHOTOS);
                params.putLong(MainFragment.BUNDLE_PARAM_DATA, id + 1);
                new OpenPreviewTask(fragmentEventListener).execute(params);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("Gallery");
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fragmentEventListener = (FragmentEventListener) activity;
    }

    class GalleryAdapter extends BaseAdapter {

        ArrayList<String> pathsToPhotos = new ArrayList<String>();

        GalleryAdapter(ArrayList<String> pathsToPhotos) {
            this.pathsToPhotos = pathsToPhotos;
        }

        @Override
        public int getCount() {
            return pathsToPhotos.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null){
                imageView = new ImageView(getActivity());
            }else {
                imageView = (ImageView) convertView;
            }

            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setAdjustViewBounds(true);
            imageView.setPadding(5,5,5,5);
            imageLoader.displayImage(pathsToPhotos.get(position),imageView,options);
            return imageView;
        }
    }


    class getUrlsTask extends AsyncTask<Void, Void, ArrayList<String>> {
        @Override
        protected ArrayList<String> doInBackground(Void... params) {
            ArrayList<String> paths = new ArrayList<String>();
            Cursor cursor = DBPhotos.getInstance().getAllDataFromTable(DBPhotos.TABLE_PHOTOS);
            if (cursor != null){
                try {
                    if (cursor.moveToFirst()){
                        do {
                            paths.add(cursor.getString(cursor.
                                    getColumnIndex(DBPhotos.COLUMN_PATH_ON_PARSE)));
                        }while (cursor.moveToNext());
                    }
                } finally {
                    cursor.close();
                }
            }
            return paths;
        }

        @Override
        protected void onPostExecute(ArrayList<String> paths) {
            super.onPostExecute(paths);
            gridView.setAdapter(new GalleryAdapter(paths));
        }
    }
}
