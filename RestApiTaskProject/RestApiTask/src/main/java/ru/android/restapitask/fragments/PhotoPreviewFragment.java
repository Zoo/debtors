package ru.android.restapitask.fragments;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;

import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.activities.MainActivity;
import ru.android.restapitask.R;

/**
 *
 * Created by dev4 on 21.02.14.
 */
public class PhotoPreviewFragment extends Fragment implements View.OnClickListener{

    ImageView imageView;
    Button bUpload,bBack;
    ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions imageLoaderOptions = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.ic_launcher)
    .bitmapConfig(Bitmap.Config.RGB_565)
    .cacheOnDisc(true)
    .cacheInMemory(true)
    .showImageOnLoading(R.drawable.load)
    .build();

    long id;
    Uri pathLocal;
    String path;
    String pathOnParse;
    String photoName;
    Double latitude;
    Double longitude;
    boolean isUploaded;
    DBPhotos dbPhotos;
    Context context;
    Bitmap photo;
    String urlClasses = "https://api.parse.com/1/classes/MyPhoto";
    String urlPhoto = "https://api.parse.com/1/files/";

    public PhotoPreviewFragment(Bundle fields) {
        this.id = fields.getLong(DBPhotos.COLUMN_ID);
        this.pathLocal = fields.getParcelable(DBPhotos.COLUMN_LOCAL_PATH);
        this.latitude = fields.getDouble(DBPhotos.COLUMN_LATITUDE);
        this.longitude = fields.getDouble(DBPhotos.COLUMN_LONGITUDE);
        this.isUploaded = fields.getInt(DBPhotos.COLUMN_IS_UPLOADED) == 1 ? true : false;
        if (isUploaded){
            path = fields.getString(DBPhotos.COLUMN_PATH_ON_PARSE);
        }
    }

    private void createPhotoName(){
        String[] splittedUri = pathLocal.toString().split("/");
        photoName = splittedUri[splittedUri.length - 1];
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("Photo preview");
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        this.context = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_photo_preview,null);
        imageView = (ImageView) view.findViewById(R.id.imageView);
        bUpload = (Button) view.findViewById(R.id.bUpload);
        bUpload.setOnClickListener(this);
        bBack = (Button) view.findViewById(R.id.bBack);
        bBack.setOnClickListener(this);
        isPhotoUploadedCheck();
        dbPhotos = DBPhotos.getInstance();

        createPhotoName();
        if (!isUploaded){
            BitmapFactory.Options options = new BitmapFactory.Options();
            photo = BitmapFactory.decodeFile(pathLocal.getPath(),options);
            imageView.setImageBitmap(photo);
        } else {
            imageLoader.displayImage(path,imageView,imageLoaderOptions);
        }
        return view;
    }


    private void isPhotoUploadedCheck(){
        if (isUploaded){
            bUpload.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bUpload :
                new uploadTask().execute(photo);
                isUploaded = true;
                isPhotoUploadedCheck();
                break;
            case R.id.bBack :
                getActivity().onBackPressed();
                break;
        }

    }

    class uploadTask extends AsyncTask<Bitmap,Void,Void>{

        @Override
        protected Void doInBackground(Bitmap... params) {
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(urlPhoto + photoName);
            HttpResponse httpResponse;
            JSONObject jsonObject;

            try {
                // Uploading photo
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 50, out);
                byte[] photoByteArray = out.toByteArray();
                ByteArrayEntity byteArrayEntity = new ByteArrayEntity(photoByteArray);

                httpPost.setHeader(MainActivity.APPLICATION_ID_NAME,MainActivity.APPLICATION_ID);
                httpPost.setHeader(MainActivity.REST_API_KEY_NAME,MainActivity.REST_API_KEY);
                httpPost.setHeader(HTTP.CONTENT_TYPE, "image/jpeg");
                httpPost.setEntity(byteArrayEntity);
                httpResponse = client.execute(httpPost);
                // Path to photo on Parse.com
                pathOnParse = httpResponse.getHeaders("Location")[0].getValue();

                // Uploading information about photo
                httpPost.setURI(URI.create(urlClasses));
                jsonObject = new JSONObject();
                jsonObject.put(DBPhotos.COLUMN_LOCAL_PATH, pathLocal);
                jsonObject.put(DBPhotos.COLUMN_PATH_ON_PARSE,pathOnParse);
                jsonObject.put(DBPhotos.COLUMN_LATITUDE,latitude);
                jsonObject.put(DBPhotos.COLUMN_LONGITUDE,longitude);
                jsonObject.put(DBPhotos.COLUMN_IS_UPLOADED, 1);
                StringEntity se = new StringEntity(jsonObject.toString());
                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                httpPost.setEntity(se);
                httpResponse = client.execute(httpPost);
                Log.d("asd","asd");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ContentValues cv = new ContentValues();
            cv.put(DBPhotos.COLUMN_LOCAL_PATH, pathLocal.toString());
            cv.put(DBPhotos.COLUMN_PATH_ON_PARSE,pathOnParse);
            cv.put(DBPhotos.COLUMN_LATITUDE,latitude);
            cv.put(DBPhotos.COLUMN_LONGITUDE,longitude);
            // Now photo uploaded, so put 1
            cv.put(DBPhotos.COLUMN_IS_UPLOADED,1);
            dbPhotos.updateRecord(DBPhotos.TABLE_PHOTOS, id, cv);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent photoUploadedIntent = new Intent(MainActivity.ACTION_RESULT_PHOTO_UPLOADED);
            photoUploadedIntent.putExtra(MainActivity.PARAM_INTENT_PHOTO_NAME,photoName);
            /**
             * parameter result is needed in case there are problems with uploading
             */
            photoUploadedIntent.putExtra(MainActivity.PARAM_INTENT_RESULT, "Uploaded !");
            context.sendBroadcast(photoUploadedIntent);
        }
    }
}
