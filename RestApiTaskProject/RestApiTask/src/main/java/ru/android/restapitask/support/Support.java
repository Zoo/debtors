package ru.android.restapitask.support;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by dev4 on 03.03.14.
 */
public class Support {

    public static boolean isConnectedToWIFI(Context context){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (!mWifi.isConnected()) {
            Toast.makeText(context, "Synchronization failed, check internet connection", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
