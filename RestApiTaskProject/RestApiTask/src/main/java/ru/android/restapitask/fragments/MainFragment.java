package ru.android.restapitask.fragments;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.R;
import ru.android.restapitask.listeners.FragmentEventListener;
import ru.android.restapitask.listeners.TaskEventListener;
import ru.android.restapitask.tasks.CheckIsDBEmptyTask;
import ru.android.restapitask.tasks.SynchronizingTask;

/**
 * Created by dev4 on 20.02.14.
 */
public class MainFragment extends Fragment implements View.OnClickListener,TaskEventListener {

    Button bMakePhoto;
    Button bShowPhoto;
    Button bShowMap;
    ImageView showImage;
    Uri pathLocal;
    Location currentLocation;
    File photoDirectory;
    DBPhotos dbPhotos;

    GalleryFragment galleryFragment;
    MapFragment mapFragment;
    LocationManager locationManager;
    LocationChangeEventListener locationChangeEventListener;
    FragmentEventListener fragmentEventListener;

    public static final int REQUEST_CODE_MAKE_PHOTO = 1;
    public static final String BUNDLE_PARAM_TABLE_NAME = "bundle_param_table_name";
    public static final String BUNDLE_PARAM_DATA = "bundle_param_data";
    public static final String BUNDLE_PARAM_TASK_RESULT = "bundle_param_task_result";
    public static final int FIRE_TASK_EVENT_ACTION = 111;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_mainfragment,null);
        bMakePhoto = (Button) view.findViewById(R.id.bMakePhoto);
        bShowPhoto = (Button) view.findViewById(R.id.bShowPhoto);
        bShowMap = (Button) view.findViewById(R.id.bShowMap);
        bMakePhoto.setOnClickListener(this);
        bShowMap.setOnClickListener(this);
        bShowPhoto.setOnClickListener(this);
        showImage = (ImageView) view.findViewById(R.id.showImage);
        locationChangeEventListener = new LocationChangeEventListener();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        mapFragment = new MapFragment();
        galleryFragment = new GalleryFragment();
        dbPhotos = DBPhotos.getInstance();
        createDirectory();
        new CheckIsDBEmptyTask(MainFragment.this).execute();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bMakePhoto :
                Intent intentMakePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                pathLocal = createImageFileUri();
                intentMakePhoto.putExtra(MediaStore.EXTRA_OUTPUT, pathLocal);
                startActivityForResult(intentMakePhoto, REQUEST_CODE_MAKE_PHOTO);
                break;
            case R.id.bShowPhoto :
                fragmentEventListener.fragmentEvent(galleryFragment);
                break;
            case R.id.bShowMap :
                fragmentEventListener.fragmentEvent(mapFragment);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_CODE_MAKE_PHOTO :
                if (resultCode == getActivity().RESULT_OK){
                    /**
                     * COLUMN_PATH_ON_PARSE is not available now, so we will add it later
                     */
                    ContentValues cv = new ContentValues();
                    cv.put(DBPhotos.COLUMN_LOCAL_PATH, pathLocal.toString());
                    cv.put(DBPhotos.COLUMN_LATITUDE, currentLocation.getLatitude());
                    cv.put(DBPhotos.COLUMN_LONGITUDE, currentLocation.getLongitude());
                    cv.put(DBPhotos.COLUMN_IS_UPLOADED, 0);

                    Bundle bundleAddNewRecordParams = new Bundle();
                    bundleAddNewRecordParams.putString(BUNDLE_PARAM_TABLE_NAME,DBPhotos.TABLE_PHOTOS);
                    bundleAddNewRecordParams.putParcelable(BUNDLE_PARAM_DATA,cv);
                    new TaskAddRecord().execute(bundleAddNewRecordParams);
                }
        }
    }

    private void createDirectory() {
        photoDirectory = new File(Environment.
                getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyFolder");
        if (!photoDirectory.exists()){
            photoDirectory.mkdirs();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentEventListener = (FragmentEventListener) activity;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActionBarActivity)getActivity()).getSupportActionBar().setIcon(R.drawable.icon_action_bar);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("Choose operation");
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 0.5F, locationChangeEventListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationChangeEventListener);
    }

    /**
     * Creating a uri of the image in local storage
     * @return  local storage uri
     */
    private Uri createImageFileUri(){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "PHOTO_" + timeStamp;
        File image = new File(photoDirectory.getPath() + "/" + imageFileName + ".jpg");
        return Uri.fromFile(image);
    }


    /**
     * Get id of the last added record and going on the preview fragment
     * @param result id of the last added record
     */
    @Override
    public void fireTaskEvent(Bundle result, int code) {
        if (code == FIRE_TASK_EVENT_ACTION){
            Boolean exist = result.getBoolean(MainFragment.BUNDLE_PARAM_DATA);
            if (!exist){
                new SynchronizingTask(getActivity()).execute();
            }
        } else {
            long id =  result.getLong(BUNDLE_PARAM_TASK_RESULT);
            Bundle fields = new Bundle();
            fields.putLong(DBPhotos.COLUMN_ID,id);
            fields.putParcelable(DBPhotos.COLUMN_LOCAL_PATH, pathLocal);
            fields.putDouble(DBPhotos.COLUMN_LATITUDE, currentLocation.getLatitude());
            fields.putDouble(DBPhotos.COLUMN_LONGITUDE,currentLocation.getLongitude());
            fields.putInt(DBPhotos.COLUMN_IS_UPLOADED, 0);
            PhotoPreviewFragment photoPreviewFragment = new PhotoPreviewFragment(fields);
            fragmentEventListener.fragmentEvent(photoPreviewFragment);
        }
    }

    @Override
    public void returnDataFromTable(Cursor cursor) {

    }

    class TaskAddRecord extends AsyncTask<Bundle,Void,Long>{

        @Override
        protected Long doInBackground(Bundle... params) {
            String tableName= params[0].getString(BUNDLE_PARAM_TABLE_NAME);
            ContentValues cv = params[0].getParcelable(BUNDLE_PARAM_DATA);
            return DBPhotos.getInstance().addRecord(tableName, cv);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            Bundle bundle = new Bundle();
            bundle.putLong(BUNDLE_PARAM_TASK_RESULT,aLong);
            fireTaskEvent(bundle, 0);
        }
    }

    class LocationChangeEventListener implements LocationListener{

        @Override
        public void onLocationChanged(Location location) {
            currentLocation = location;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }
}
