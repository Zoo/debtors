package ru.android.restapitask.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.File;

import ru.android.restapitask.R;
import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.fragments.MainFragment;
import ru.android.restapitask.listeners.FragmentEventListener;

public class MainActivity extends ActionBarActivity implements FragmentEventListener {

    ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options;
    BroadcastReceiver broadcastReceiver;

    public static final String APPLICATION_ID_NAME = "X-Parse-Application-Id";
    public static final String APPLICATION_ID = "HR1n2Bl5uGuNpkwKgBgi2TBTq4SyifOS0LsuTIll";
    public static final String REST_API_KEY_NAME = "X-Parse-REST-API-Key";
    public static final String REST_API_KEY = "8LoTnJjB1QnWYwwEejdnPg2YFNgjPpoDtbEwqjhX";

    public static final String ACTION_RESULT_PHOTO_UPLOADED = "action_result_photo_uploaded";
    public static final String PARAM_INTENT_RESULT = "param_intent_result";
    public static final String PARAM_INTENT_PHOTO_NAME = "param_intent_photo_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .cacheOnDisc(true)
                .cacheInMemory(true)
                .showImageOnLoading(R.drawable.load)
                .build();

        /**
         * Deleting database
         * ///////////////////////////////////////
         * //////////DEVELOPERS ONLY :)///////////
         * ///////////////////////////////////////
         */
        deleteDatabase(DBPhotos.DB_NAME);
        DBPhotos.getInstance().initializeContext(this);
        DBPhotos.getInstance().open();
        File cacheDir = getCacheDir();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(320, 480) // width, height
                .discCacheExtraOptions(320, 480, Bitmap.CompressFormat.JPEG, 75, null) // width, height, compress format, quality
                .threadPoolSize(5)
                .threadPriority(Thread.MIN_PRIORITY + 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) // 2 Mb
                .discCache(new UnlimitedDiscCache(cacheDir))
                .imageDownloader(new BaseImageDownloader(this, 5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        if (!imageLoader.isInited()){
            imageLoader.init(config);
        }


        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(MainActivity.this,"Photo: " + intent.getStringExtra(PARAM_INTENT_PHOTO_NAME)
                        + " " + intent.getStringExtra(PARAM_INTENT_RESULT),Toast.LENGTH_SHORT).show();
            }
        };
        getSupportActionBar().setHomeButtonEnabled(true);
        Fragment mainFragment = new MainFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.mainActivityContainer,mainFragment);
        ft.commit();
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver,new IntentFilter(ACTION_RESULT_PHOTO_UPLOADED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home :
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void fragmentEvent(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainActivityContainer,fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DBPhotos.getInstance().close();
    }
}
