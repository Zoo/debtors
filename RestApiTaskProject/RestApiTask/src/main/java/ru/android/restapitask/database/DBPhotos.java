package ru.android.restapitask.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by dev4 on 20.02.14.
 */
public class DBPhotos {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "PhotosParse";
    public static final String TABLE_PHOTOS = "Photos";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LOCAL_PATH = "pathLocal";
    public static final String COLUMN_PATH_ON_PARSE = "pathOnParse";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE= "longitude";
    public static final String COLUMN_IS_UPLOADED = "isUploaded";

    DBHelper dbHelper;
    Context context;
    SQLiteDatabase photosDB;

    private static DBPhotos instance;

    public static synchronized DBPhotos getInstance() {
        if (instance == null) {
            instance = new DBPhotos();
        }
        return instance;
    }

    public void initializeContext(Context context){
        this.context = context;
    }

    public void open(){
        if (dbHelper == null){
            dbHelper = new DBHelper(context,DB_NAME);
            photosDB = dbHelper.getWritableDatabase();
        }
    }

    public void close(){
        if (dbHelper != null){
            dbHelper.close();
            dbHelper= null;
        }
    }

    public Cursor getAllDataFromTable(String table_name){
        return photosDB.query(table_name, null, null, null, null, null, null);
    }

    public void deleteRecord(String table_name, long id){
        photosDB.delete(table_name, "_id = " + id, null);
    }

    public long addRecord(String table_name, ContentValues contentValues){
        return photosDB.insert(table_name, null, contentValues);
    }

    public void updateRecord(String table_name, long id, ContentValues contentValues){
        photosDB.update(table_name, contentValues, "_id = " + id, null);
    }

    public Cursor getRecord(String tableName,long id){
        return photosDB.query(tableName, null, "_id = " + id, null, null , null, null);
    }

    private class DBHelper extends SQLiteOpenHelper{

        public DBHelper(Context context, String name){
            super(context, name, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE Photos (" + COLUMN_ID +" integer primary key autoincrement, " +
                                                 COLUMN_LOCAL_PATH + " text, " +
                                                 COLUMN_PATH_ON_PARSE + " text," +
                                                 COLUMN_LATITUDE + " float," +
                                                 COLUMN_LONGITUDE + " float, " +
                                                 COLUMN_IS_UPLOADED + " integer);");
        }

        @Override
        public void onOpen(SQLiteDatabase db) {
            super.onOpen(db);
            db.execSQL("PRAGMA foreign_keys=ON;");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
