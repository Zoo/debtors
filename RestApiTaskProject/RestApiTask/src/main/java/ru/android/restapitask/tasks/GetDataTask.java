package ru.android.restapitask.tasks;

import android.database.Cursor;
import android.os.AsyncTask;

import ru.android.restapitask.database.DBPhotos;
import ru.android.restapitask.listeners.TaskEventListener;

/**
 * Created by dev4 on 26.02.14.
 */
public class GetDataTask extends AsyncTask<Void,Void,Cursor> {
    TaskEventListener taskEventListener;

    public GetDataTask(TaskEventListener taskEventListener) {
        this.taskEventListener = taskEventListener;
    }

    @Override
    protected Cursor doInBackground(Void... params) {
        Cursor c = DBPhotos.getInstance().getAllDataFromTable(DBPhotos.TABLE_PHOTOS);
        return c;
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        super.onPostExecute(cursor);
        taskEventListener.returnDataFromTable(cursor);
        if (cursor != null){
            cursor.close();
        }
    }
}
