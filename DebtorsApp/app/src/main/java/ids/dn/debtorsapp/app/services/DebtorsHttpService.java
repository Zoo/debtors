package ids.dn.debtorsapp.app.services;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.Support;
import ids.dn.debtorsapp.app.tasks.DeleteRecordTask;
import ids.dn.debtorsapp.app.tasks.SynchronizeTask;
import ids.dn.debtorsapp.app.tasks.update.UpdateDebtMoneyTask;
import ids.dn.debtorsapp.app.tasks.update.UpdateDebtStuffTask;
import ids.dn.debtorsapp.app.tasks.update.UpdateDebtorTask;
import ids.dn.debtorsapp.app.tasks.upload.UploadDebtTask;
import ids.dn.debtorsapp.app.tasks.upload.UploadDebtorTask;

/**
 * Created by dev4 on 28.02.14.
 */
public class DebtorsHttpService extends Service{

    ServiceBinder binder = new ServiceBinder();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private boolean checkConnection(){
        if (!Support.checkConnection(getApplicationContext())){
            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void uploadDebtor(ContentValues cv){
        if (!checkConnection()) return;
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.PARAM_DATA, cv);
        UploadDebtorTask task = new UploadDebtorTask(getApplicationContext());
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bundle);
        }
        else {
            task.execute(bundle);
        }
    }

    public void uploadDebt(ContentValues cv){
        if (!checkConnection()) return;
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.PARAM_DATA, cv);
        UploadDebtTask task = new UploadDebtTask(getApplicationContext());
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bundle);
        }
        else {
            task.execute(bundle);
        }
    }

    public void updateDebtMoney(ContentValues cv){
        if (!checkConnection()) return;
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.PARAM_DATA, cv);
        UpdateDebtMoneyTask task = new UpdateDebtMoneyTask(getApplicationContext());
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bundle);
        }
        else {
            task.execute(bundle);
        }
    }

    public void updateDebtStuff(ContentValues cv){
        if (!checkConnection()) return;
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.PARAM_DATA, cv);
        UpdateDebtStuffTask task = new UpdateDebtStuffTask(getApplicationContext());
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bundle);
        }
        else {
            task.execute(bundle);
        }
    }

    public void delete(ContentValues cv){
        if (!checkConnection()) return;
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.PARAM_DATA,cv);
        DeleteRecordTask task = new DeleteRecordTask(getApplicationContext());
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bundle);
        }
        else {
            task.execute(bundle);
        }
    }

    public void synchronizeData(String userObjectId){
        if (!checkConnection()) return;
        SynchronizeTask task = new SynchronizeTask(getApplicationContext());
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, userObjectId);
        }
        else {
            task.execute(userObjectId);
        }
    }

    public void updateDebtor(ContentValues cv){
        if (!checkConnection()) return;
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.PARAM_DATA,cv);
        UpdateDebtorTask task = new UpdateDebtorTask(getApplicationContext());
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bundle);
        }
        else {
            task.execute(bundle);
        }
    }

    public class ServiceBinder extends Binder{
        public DebtorsHttpService getService(){
            return DebtorsHttpService.this;
        }
    }
}
