package ids.dn.debtorsapp.app.tasks.update;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import java.io.IOException;

import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.GSONClasses.Debt;
import ids.dn.debtorsapp.app.support.GSONClasses.Pointer;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 13.03.14.
 */
public class UpdateDebtStuffTask extends AsyncTask<Bundle, ContentValues, ContentValues> {

    Context context;

    public UpdateDebtStuffTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onProgressUpdate(ContentValues... values) {
        super.onProgressUpdate(values);
        DBDebtors.getInstance().updateRecord(Constants.DB_TABLE_DEBTS,
                values[0].getAsLong(Constants.COLUMN_ID), values[0]);
        Intent dbUpdatedIntent = new Intent(Constants.ACTION_RESULT_DB_UPDATED);
        context.sendBroadcast(dbUpdatedIntent);
    }

    @Override
    protected ContentValues doInBackground(Bundle... params) {
        ContentValues cv = params[0].getParcelable(Constants.PARAM_DATA);
        String objectId = cv.getAsString(Constants.COLUMN_OBJECT_ID);
        String path = cv.getAsString(Constants.COLUMN_PHOTO);

        publishProgress(cv);
        HttpClient client = new DefaultHttpClient();
        HttpPut updateRequest = new HttpPut(Constants.URI_CLASS_DEBT + "/" + objectId);
        updateRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
        updateRequest.setHeader(Constants.REST_API_KEY_NAME,Constants.REST_API_KEY);

        String pathOnParse = cv.getAsString(Constants.COLUMN_PHOTO_PARSE_PATH);
        try {
            if ("".equals(pathOnParse)){
                pathOnParse = Support.uploadPhoto(path,client,context.getContentResolver());
            }
            String name = cv.getAsString(Constants.COLUMN_NAME);
            String dateLoan = cv.getAsString(Constants.COLUMN_DATE_LOAN);
            String dateReturn = cv.getAsString(Constants.COLUMN_DATE_RETURN);
            Pointer debtor = new Pointer("Pointer","Debtor",cv.getAsString(Constants.COLUMN_POINTER_DEBTOR));
            long remind_event_id = cv.getAsLong(Constants.COLUMN_REMIND_EVENT_ID);
            Debt debt = new Debt(0, dateLoan, dateReturn, debtor, pathOnParse, path, 0, name, remind_event_id);

            // Update model
            Gson gson = new Gson();
            String json = gson.toJson(debt);
            Support.updateModel(json, updateRequest,client);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Update DB record
        cv.put(Constants.COLUMN_PHOTO_PARSE_PATH, pathOnParse);
        return cv;
    }

    @Override
    protected void onPostExecute(ContentValues values) {
        super.onPostExecute(values);
        DBDebtors.getInstance().updateRecord(Constants.DB_TABLE_DEBTS,
                values.getAsLong(Constants.COLUMN_ID), values);
        Intent updateDebtMoneyIntent = new Intent(Constants.ACTION_RESULT_DEBT_UPDATED);
        context.sendBroadcast(updateDebtMoneyIntent);
    }
}
