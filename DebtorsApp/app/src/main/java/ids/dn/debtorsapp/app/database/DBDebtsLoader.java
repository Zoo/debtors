package ids.dn.debtorsapp.app.database;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import ids.dn.debtorsapp.app.support.Constants;

/**
 * Created by dev4 on 06.03.14.
 */
public class DBDebtsLoader extends CursorLoader {

    String debtorObjectId;

    public DBDebtsLoader(Context context, String debtorObjectId) {
        super(context);
        this.debtorObjectId = debtorObjectId;
    }

    @Override
    public Cursor loadInBackground() {
        String query = "SELECT * FROM " + Constants.DB_TABLE_DEBTS +
                " WHERE " + Constants.COLUMN_POINTER_DEBTOR + " = " + "'" + debtorObjectId + "'";
        Cursor cursor = DBDebtors.getInstance().getDataByRawQuery(query);
        return cursor;
    }
}
