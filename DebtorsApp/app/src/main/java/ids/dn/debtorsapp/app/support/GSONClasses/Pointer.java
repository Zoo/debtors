package ids.dn.debtorsapp.app.support.GSONClasses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dev4 on 06.03.14.
 */
public class Pointer {
    @SerializedName("__type")
    public String type;
    @SerializedName("className")
    public String className;
    @SerializedName("objectId")
    public String objectId;

    public Pointer(String type, String className, String objectId) {
        this.type = type;
        this.className = className;
        this.objectId = objectId;
    }
}