package ids.dn.debtorsapp.app.tasks.upload;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;

import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.GSONClasses.Debtor;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 04.03.14.
 */
public class UploadDebtorTask extends AsyncTask<Bundle, ContentValues, ContentValues> {

    Context context;
    long id;

    public UploadDebtorTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onProgressUpdate(ContentValues... values) {
        super.onProgressUpdate(values);
        id = DBDebtors.getInstance().addRecord(Constants.DB_TABLE_DEBTORS, values[0]);
        Intent debtorAddedIntent = new Intent(Constants.ACTION_RESULT_DEBTOR_ADDED);
        context.sendBroadcast(debtorAddedIntent);
    }

    @Override
    protected ContentValues doInBackground(Bundle... params) {
        ContentValues cv = params[0].getParcelable(Constants.PARAM_DATA);
        String path = cv.getAsString(Constants.COLUMN_PHOTO);
        publishProgress(cv);
        //creating HTTP parameters
        HttpClient client = new DefaultHttpClient();
        String photoName = path == null ? "" : Support.getPhotoName(path);
        HttpPost postRequest = new HttpPost(Constants.URI_FILE_PATHS + photoName);
        postRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
        postRequest.setHeader(Constants.REST_API_KEY_NAME,Constants.REST_API_KEY);
        //creating photo
        Bitmap photo = Support.getBitmap(path,context.getContentResolver(),true);
        ByteArrayEntity byteArrayEntity = null;
        String objectId;
        if (photo != null){
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG,100,bos);
            byte[] bytePhotoArray = bos.toByteArray();
            byteArrayEntity = new ByteArrayEntity(bytePhotoArray);
        }
        try {
            // Uploading photo
            String pathOnParse = photo == null ? "" : Support.uploadFile(postRequest, client, byteArrayEntity);
            // Uploading model
            Gson gson = new Gson();
            Debtor debtor = new Debtor(cv.getAsString(Constants.COLUMN_NAME),
                    cv.getAsString(Constants.COLUMN_SURNAME),
                    cv.getAsString(Constants.COLUMN_TELEPHONE_NUMBER),
                    cv.getAsString(Constants.COLUMN_EMAIL),
                    pathOnParse,
                    cv.getAsString(Constants.COLUMN_USER));
            String json = gson.toJson(debtor);
            postRequest.setURI(URI.create(Constants.URI_CLASS_DEBTOR));
            // Adding new record to Database
            objectId = Support.uploadModel(json, postRequest, client);
            cv.put(Constants.COLUMN_OBJECT_ID,objectId);
            cv.put(Constants.COLUMN_PHOTO_PARSE_PATH, pathOnParse);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cv;
    }

    @Override
    protected void onPostExecute(ContentValues values) {
        super.onPostExecute(values);
        DBDebtors.getInstance().updateRecord(Constants.DB_TABLE_DEBTORS, id, values);
        Intent photoUploadedIntent = new Intent(Constants.ACTION_RESULT_PHOTO_UPLOADED);
        context.sendBroadcast(photoUploadedIntent);
    }
}