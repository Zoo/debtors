package ids.dn.debtorsapp.app.tasks;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.database.DBDebtors;

/**
 * Created by dev4 on 28.02.14.
 */
public class AddRecordTask extends AsyncTask<Bundle,Void,Void> {


    public AddRecordTask(Context ctx) {
    }

    @Override
    protected Void doInBackground(Bundle... params) {
        String tableName = params[0].getString(Constants.PARAM_TABLE_NAME);
        ContentValues data = params[0].getParcelable(Constants.PARAM_DATA);
        DBDebtors.getInstance().addRecord(tableName, data);
        return null;
    }
}
