package ids.dn.debtorsapp.app.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.File;

import ids.dn.debtorsapp.app.R;
import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.fragments.FragmentDebtors;
import ids.dn.debtorsapp.app.listeners.FragmentListener;
import ids.dn.debtorsapp.app.support.Constants;

/**
 * Created by dev4 on 28.02.14.
 */
public class DebtorsListActivity extends ActionBarActivity implements FragmentListener{

    String userObjectId;
    ImageLoader imageLoader = ImageLoader.getInstance();
    File photoDirectory;
    public static String photoDirectoryPath;
    MenuItem syncItem = null;
    static final int MENU_REFRESH = 1112;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        createDirectory(Constants.IMAGE_FOLDER_NAME);
        File cacheDir = getCacheDir();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(320, 480) // width, height
                .discCacheExtraOptions(320, 480, Bitmap.CompressFormat.JPEG, 75, null) // width, height, compress format, quality
                .threadPoolSize(5)
                .threadPriority(Thread.MIN_PRIORITY + 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) // 2 Mb
                .discCache(new UnlimitedDiscCache(cacheDir))
                .imageDownloader(new BaseImageDownloader(this, 5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        if (!imageLoader.isInited()){
            imageLoader.init(config);
        }
        // Getting users id
        userObjectId = getIntent().getExtras().getString(Constants.PARAM_DATA);
        deleteDatabase(Constants.DB_NAME);
        DBDebtors.getInstance().create(getApplicationContext());
        photoDirectoryPath = photoDirectory.getPath();
        if (savedInstanceState == null){
            FragmentDebtors debtorsList = new FragmentDebtors();
            debtorsList.setUserObjectId(userObjectId);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.ListActivityContainer, debtorsList);
            fragmentTransaction.commit();
        }

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (Constants.ACTION_RESULT_SYNCHRONIZE_BEGINS.equals(action)){
                    runSync();
                }
                if (Constants.ACTION_RESULT_SYNCHRONIZE_CANCELED.equals(action)){
                    stopSync();
                }
                if (Constants.ACTION_RESULT_SYNCHRONIZE_COMPLETED.equals(action)){
                    stopSync();
                }
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        menu.add(Menu.NONE, MENU_REFRESH, 1, "Refresh data")
                .setIcon(R.drawable.sync);
        syncItem = menu.findItem(MENU_REFRESH);
        MenuItemCompat.setShowAsAction(syncItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        stopSync();
        return true;
    }

    // ======================================
    // Synchronization icon animation methods
    // ======================================

    public void stopSync() {
        if (syncItem != null) {
            syncItem.setVisible(false);
        }
    }

    public void runSync() {
        if (syncItem != null) {
            syncItem.setVisible(true);
            MenuItemCompat.setActionView(syncItem,R.layout.indeterminate_progress_action);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :
                getSupportFragmentManager().popBackStack();
                break;
            case R.id.menu_item_logout :
                openLogoutDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openLogoutDialog(){
        AlertDialog.Builder logoutDialog = new AlertDialog.Builder(this);
        logoutDialog.setTitle(R.string.logout);
        logoutDialog.setPositiveButton(R.string.answer_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor ed = DebtorsListActivity.this.getSharedPreferences("pref", MODE_PRIVATE).edit();
                ed.putString(Constants.LOGIN, "");
                ed.putString(Constants.PASSWORD, "");
                ed.commit();
                Intent loginActivityIntent = new Intent(DebtorsListActivity.this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
            }
        });
        logoutDialog.setNegativeButton(R.string.answer_no, null);
        logoutDialog.show();
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ListActivityContainer, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_RESULT_SYNCHRONIZE_BEGINS);
        intentFilter.addAction(Constants.ACTION_RESULT_SYNCHRONIZE_COMPLETED);
        intentFilter.addAction(Constants.ACTION_RESULT_SYNCHRONIZE_CANCELED);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private void createDirectory(String name) {
        photoDirectory = new File(Environment.
                getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                name);
        if (!photoDirectory.exists()){
            photoDirectory.mkdirs();
        }
    }
}
