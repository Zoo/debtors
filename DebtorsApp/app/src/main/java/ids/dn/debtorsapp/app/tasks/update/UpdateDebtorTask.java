package ids.dn.debtorsapp.app.tasks.update;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import java.io.IOException;

import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.support.GSONClasses.Debtor;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 28.02.14.
 */
public class UpdateDebtorTask extends AsyncTask<Bundle,ContentValues,ContentValues> {

    Context context;

    public UpdateDebtorTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onProgressUpdate(ContentValues... values) {
        super.onProgressUpdate(values);
        DBDebtors.getInstance().updateRecord(Constants.DB_TABLE_DEBTORS,values[0].getAsLong(Constants.COLUMN_ID),values[0]);
        Intent dbUpdatedIntent = new Intent(Constants.ACTION_RESULT_DB_UPDATED);
        context.sendBroadcast(dbUpdatedIntent);
    }

    @Override
    protected ContentValues doInBackground(Bundle... params) {
        ContentValues cv = params[0].getParcelable(Constants.PARAM_DATA);
        publishProgress(cv);
        //get local path to photo
        String path = cv.getAsString(Constants.COLUMN_PHOTO);
        //creating put request to update existing debtor
        HttpClient client = new DefaultHttpClient();
        HttpPut updateRequest = new HttpPut(Constants.URI_CLASS_DEBTOR +
                "/" + cv.getAsString(Constants.COLUMN_OBJECT_ID));
        updateRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
        updateRequest.setHeader(Constants.REST_API_KEY_NAME,Constants.REST_API_KEY);
        //creating empty post request
        String pathOnParse = cv.getAsString(Constants.COLUMN_PHOTO_PARSE_PATH);
        try {
            if ("".equals(pathOnParse)){
               pathOnParse = Support.uploadPhoto(path,client,context.getContentResolver());
            }
            // Uploading model
            Gson gson = new Gson();
            Debtor debtor = new Debtor(cv.getAsString(Constants.COLUMN_NAME),
                    cv.getAsString(Constants.COLUMN_SURNAME),
                    cv.getAsString(Constants.COLUMN_TELEPHONE_NUMBER),
                    cv.getAsString(Constants.COLUMN_EMAIL),
                    pathOnParse,
                    cv.getAsString(Constants.COLUMN_USER));
            String json = gson.toJson(debtor);
            // Update DB
            Support.updateModel(json,updateRequest,client);
        }catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        cv.put(Constants.COLUMN_PHOTO_PARSE_PATH, pathOnParse);
        return cv;
    }

    @Override
    protected void onPostExecute(ContentValues values) {
        super.onPostExecute(values);
        DBDebtors.getInstance().updateRecord(Constants.DB_TABLE_DEBTORS,values.getAsLong(Constants.COLUMN_ID),values);
        Intent updatedIntent = new Intent(Constants.ACTION_RESULT_DEBTOR_UPDATED);
        context.sendBroadcast(updatedIntent);
    }
}
