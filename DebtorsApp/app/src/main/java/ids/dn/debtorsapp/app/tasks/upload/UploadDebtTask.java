package ids.dn.debtorsapp.app.tasks.upload;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;

import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.GSONClasses.Debt;
import ids.dn.debtorsapp.app.support.GSONClasses.Pointer;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 06.03.14.
 */
public class UploadDebtTask extends AsyncTask<Bundle, ContentValues, ContentValues>{

    Context context;
    long id;

    public UploadDebtTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onProgressUpdate(ContentValues... values) {
        super.onProgressUpdate(values);
        //adding new record to dataBase
        id = DBDebtors.getInstance().addRecord(Constants.DB_TABLE_DEBTS, values[0]);
        Intent debtorAddedIntent = new Intent(Constants.ACTION_RESULT_DEBT_ADDED);
        context.sendBroadcast(debtorAddedIntent);
    }

    @Override
    protected ContentValues doInBackground(Bundle... params) {
        ContentValues cv = params[0].getParcelable(Constants.PARAM_DATA);
        publishProgress(cv);
        //check type of debt
        int debtType = cv.getAsInteger(Constants.COLUMN_IS_DEBT_MONEY);
        //create http request and client
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost();
        Gson gson = new Gson();
        String json;
        //set authorization parameters to request header
        post.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
        post.setHeader(Constants.REST_API_KEY_NAME,Constants.REST_API_KEY);
        //check wifi connection
        try {
            /**
             * Creating Pointer to debtor on parse.com
             */
            String objectId = null;
            Pointer debtor = new Pointer("Pointer","Debtor",cv.getAsString(Constants.COLUMN_POINTER_DEBTOR));
            switch (debtType){
                case Constants.DEBT_TYPE_MONEY :
                    post.setURI(URI.create(Constants.URI_CLASS_DEBT));
                    Debt debtMoney = new Debt(cv.getAsInteger(Constants.COLUMN_AMOUNT),
                            cv.getAsString(Constants.COLUMN_DATE_LOAN),
                            cv.getAsString(Constants.COLUMN_DATE_RETURN),
                            debtor,
                            "",
                            "",
                            debtType,
                            "",
                            cv.getAsLong(Constants.COLUMN_REMIND_EVENT_ID));
                    json = gson.toJson(debtMoney);
                    //upload model
                    objectId = Support.uploadModel(json,post,client);
                    break;
                case Constants.DEBT_TYPE_STUFF :
                    String pathToPhoto = cv.getAsString(Constants.COLUMN_PHOTO);
                    String photoName = TextUtils.isEmpty(pathToPhoto) ? "" : Support.getPhotoName(pathToPhoto);
                    post.setURI(URI.create(Constants.URI_FILE_PATHS + photoName));
                    Bitmap photo = Support.getBitmap(pathToPhoto, context.getContentResolver(), true);

                    ByteArrayEntity byteArrayEntity = null;
                    if (photo != null) {
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.JPEG, 50, bos);
                        byte[] bytePhotoArray = bos.toByteArray();
                        byteArrayEntity = new ByteArrayEntity(bytePhotoArray);
                    }
                    //upload photo
                    String pathOnParse = photo == null ? "" : Support.uploadFile(post, client, byteArrayEntity);
                    post.setURI(URI.create(Constants.URI_CLASS_DEBT));
                    //create model of debt
                    Debt debtStuff = new Debt(0,
                            cv.getAsString(Constants.COLUMN_DATE_LOAN),
                            cv.getAsString(Constants.COLUMN_DATE_RETURN),
                            debtor,
                            pathOnParse,
                            pathToPhoto,
                            debtType,
                            cv.getAsString(Constants.COLUMN_NAME),
                            cv.getAsLong(Constants.COLUMN_REMIND_EVENT_ID));
                    json = gson.toJson(debtStuff);
                    //upload model
                    objectId = Support.uploadModel(json, post, client);
                    //put path on parse.com to content values
                    cv.put(Constants.COLUMN_PHOTO_PARSE_PATH, pathOnParse);
                    break;
            }
            cv.put(Constants.COLUMN_OBJECT_ID, objectId);
        }catch (IOException e){
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cv;
    }

    @Override
    protected void onPostExecute(ContentValues values) {
        super.onPostExecute(values);
        DBDebtors.getInstance().updateRecord(Constants.DB_TABLE_DEBTS, id, values);
        Intent photoUploadedIntent = new Intent(Constants.ACTION_RESULT_PHOTO_UPLOADED);
        context.sendBroadcast(photoUploadedIntent);
    }
}
