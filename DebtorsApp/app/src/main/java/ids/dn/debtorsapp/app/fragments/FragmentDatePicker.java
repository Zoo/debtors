package ids.dn.debtorsapp.app.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ids.dn.debtorsapp.app.listeners.FragmentListener;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 05.03.14.
 */
public class FragmentDatePicker extends DialogFragment {

    EditText editText;

    public FragmentDatePicker(EditText editText) {
        this.editText = editText;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        if (!TextUtils.isEmpty(editText.getText().toString())){
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy");
                Date date = sdf.parse(editText.getText().toString());
                c.setTime(date);
            } catch (ParseException e) {
                Log.d("<-- PARSING ERROR -->", "Error while parsing date");
            }
        }
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(),myCallback, year, month, day);
    }


    DatePickerDialog.OnDateSetListener myCallback = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            editText.setText((dayOfMonth + "." + (monthOfYear + 1) + "." + year));
        }
    };
}
