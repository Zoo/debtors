package ids.dn.debtorsapp.app.listeners;

import android.database.Cursor;

public interface TaskListener {
    public void fireTaskComplete(Cursor cursor);
    public void returnString(String value);
}
