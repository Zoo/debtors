package ids.dn.debtorsapp.app.listeners;

import android.support.v4.app.Fragment;

/**
 * Created by dev4 on 28.02.14.
 */
public interface FragmentListener {
    public void replaceFragment(Fragment fragment);
}
