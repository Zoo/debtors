package ids.dn.debtorsapp.app.support.GSONClasses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dev4 on 20.03.14.
 */
public class BatchDeletingObject {

    @SerializedName("method")
    String method;
    @SerializedName("path")
    String path;

    public BatchDeletingObject(String method, String path) {
        this.method = method;
        this.path = path;
    }
}
