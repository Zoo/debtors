package ids.dn.debtorsapp.app.tasks.update;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.facebook.Settings;
import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import java.io.IOException;

import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.GSONClasses.Debt;
import ids.dn.debtorsapp.app.support.GSONClasses.Pointer;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 13.03.14.
 */
public class UpdateDebtMoneyTask extends AsyncTask<Bundle, ContentValues, Void> {

    Context context;

    public UpdateDebtMoneyTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onProgressUpdate(ContentValues... values) {
        super.onProgressUpdate(values);
        //Updating DB record
        DBDebtors.getInstance().updateRecord(Constants.DB_TABLE_DEBTS,
                values[0].getAsLong(Constants.COLUMN_ID), values[0]);
        Intent dbUpdatedIntent = new Intent(Constants.ACTION_RESULT_DB_UPDATED);
        context.sendBroadcast(dbUpdatedIntent);
    }

    @Override
    protected Void doInBackground(Bundle... params) {
        ContentValues cv = params[0].getParcelable(Constants.PARAM_DATA);
        String objectId = cv.getAsString(Constants.COLUMN_OBJECT_ID);
        publishProgress(cv);
        // Creating HTTP request
        HttpClient client = new DefaultHttpClient();
        HttpPut updateRequest = new HttpPut(Constants.URI_CLASS_DEBT + "/" + objectId);
        updateRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
        updateRequest.setHeader(Constants.REST_API_KEY_NAME,Constants.REST_API_KEY);

        // Getting all needed fields
        int amount = cv.getAsInteger(Constants.COLUMN_AMOUNT);
        String dateLoan = cv.getAsString(Constants.COLUMN_DATE_LOAN);
        String dateReturn = cv.getAsString(Constants.COLUMN_DATE_RETURN);
        long remind_event_id = cv.getAsLong(Constants.COLUMN_REMIND_EVENT_ID);
        Pointer debtor = new Pointer("Pointer","Debtor",cv.getAsString(Constants.COLUMN_POINTER_DEBTOR));
        Debt debt = new Debt(amount, dateLoan, dateReturn, debtor, "", "", 1, "", remind_event_id);

        //Updating money debt
        Gson gson = new Gson();
        String json = gson.toJson(debt);
        try {
            Support.updateModel(json, updateRequest, client);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Intent updateDebtMoneyIntent = new Intent(Constants.ACTION_RESULT_DEBT_UPDATED);
        context.sendBroadcast(updateDebtMoneyIntent);
    }
}
