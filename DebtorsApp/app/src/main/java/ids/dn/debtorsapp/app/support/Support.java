package ids.dn.debtorsapp.app.support;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dev4 on 04.03.14.
 */
public class Support {
    /**
     * Parse inputStream to JSONObject
     * @param inputStream inputStream from which to create JSONObject
     * @return
     */
    public static JSONObject parseToJSONObject(InputStream inputStream){
        JSONObject jsonObject = null;
        try {
            byte[] temp = new byte[1024];
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int read;
            while ((read = inputStream.read(temp)) != -1){
                bos.write(temp,0,read);
            }
            inputStream.close();
            String JSONString = new String(bos.toByteArray());
            jsonObject = new JSONObject(JSONString);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     * Check Wi-Fi connection
     * @param context
     * @return Boolean is connected
     */
    public static boolean checkConnection(Context context){
        ConnectivityManager conMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);;
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }

    /**
     * Getting real path from uri
     * @param contentUri Uri to file
     * @param contentResolver Content resolver
     * @return Real path to file
     */
    public static String getRealPathFromURI(Uri contentUri, ContentResolver contentResolver) {
        Cursor cursor = null;
        try {
            String[] columns = { MediaStore.Images.Media.DATA };
            cursor = contentResolver.query(contentUri,  columns, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * Getting bitmap from path
     * @param path path to photo (can be from contacts of from SD card)
     * @param contentResolver content resolver
     * @return Bitmap if we could create photo or null if we couldn't
     */
    public static Bitmap getBitmap(String path, ContentResolver contentResolver, boolean isScale){
        Bitmap bitmap;
        try {
            /**
             * Trying to create and scale photo from SD card
             */
            bitmap = decodePhotoSDCard(path, isScale);
            } catch (IOException e1) {
            /**
             * If photo was not created - trying to create from contacts
             * (not scaling it, because contact's photo already scaled)
             */
            try {
                InputStream is = contentResolver.openInputStream(Uri.parse(path));
                bitmap = BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                /**
                 * If photo from contacts was not created - path is not correct or equals NULL
                 */
                return null;
            }
            /**
             * Path is not correct or equals NULL - return null
             */
        } catch (NullPointerException e){
            return null;
        }
        return bitmap;
    }

    /**
     * Decode photo from SD card and scale it
     * @param path path to photo
     * @return Photo in bitmap format
     * @throws IOException
     * @throws NullPointerException
     */
    public static Bitmap decodePhotoSDCard(String path, boolean isScale) throws IOException,NullPointerException {
        Bitmap b;
        if (isScale){
            //Decode image size
            File f = new File(path);
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();

            int IMAGE_MAX_SIZE = 125;
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE /
                        (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
        } else {
            b = BitmapFactory.decodeFile(path);
        }
        return b;
    }

    /**
     * Upload file to Parse.com
     * @param postRequest HTTP Post request
     * @param client HTTP Client
     * @param byteArrayEntity File represented in ByteArrayEntity
     * @return Path to photo on Parse.com
     * @throws IOException
     */
    public static String uploadFile(HttpPost postRequest,HttpClient client, ByteArrayEntity byteArrayEntity) throws IOException {
        postRequest.setHeader(HTTP.CONTENT_TYPE,"image/jpeg");
        postRequest.setEntity(byteArrayEntity);
        HttpResponse response = client.execute(postRequest);
        return response.getHeaders("Location")[0].getValue();
    }

    /**
     * Creating new user on Parse.com
     * @param JSONObject json object with data
     * @param postRequest post request
     * @param client HTTP Client
     * @return objectId of created user
     * @throws IOException
     * @throws JSONException
     */
    public static final String createNewUser(String JSONObject, HttpPost postRequest,HttpClient client) throws IOException, JSONException {
        String userObjectId = null;
        StringEntity se = new StringEntity(JSONObject);
        postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
        postRequest.setEntity(se);
        HttpResponse response = client.execute(postRequest);
        //201 = Created successful
        if (response.getStatusLine().getStatusCode() == 201){
            String location = response.getHeaders("Location")[0].getValue();
            String[] locationArray = location.split("/");
            userObjectId = locationArray[locationArray.length - 1];
        }
        return userObjectId;
    }

    /**
     * Upload model to Parse.com
     * @param JSONObject JSON Object containing model parameters
     * @param postRequest HTTP Post request
     * @param client HTTP Client
     * @return ObjectId of just uploaded record
     * @throws IOException
     * @throws JSONException
     */
    public static String uploadModel(String JSONObject, HttpPost postRequest,HttpClient client) throws IOException, JSONException {
        StringEntity se = new StringEntity(JSONObject,HTTP.UTF_8);
        postRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
        postRequest.setEntity(se);
        HttpResponse response = client.execute(postRequest);
        // Getting objectId from just added record
        JSONObject responseJSONObj = Support.parseToJSONObject(response.getEntity().getContent());
        String objectId = responseJSONObj.getString(Constants.COLUMN_OBJECT_ID);
        return objectId;
    }

    /**
     * Updating already existing record
     * @param JSONObject
     * @param updateRequest
     * @param client
     * @throws IOException
     * @throws JSONException
     */
    public static void updateModel(String JSONObject, HttpPut updateRequest,HttpClient client) throws IOException, JSONException {
        StringEntity se = new StringEntity(JSONObject,HTTP.UTF_8);
        updateRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
        updateRequest.setEntity(se);
        client.execute(updateRequest);
    }

    /**
     * Create name of photo by it's path
     * @param path path to photo
     * @return photo name
     */
    public static String getPhotoName(String path){
        String[] temp = path.split("/");
        String photoName = temp[temp.length - 1];
        // If photo came from contacts - it does not have extension
        // so we add EXTENSION_JPG = ".jpg" and timestamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        if (!photoName.endsWith(Constants.EXTENSION_JPG)){
            photoName = photoName + timeStamp + Constants.EXTENSION_JPG;
        }
        return photoName;
    }

    /**
     * Uploading photo to Parse.com
     * @param path local path to photo
     * @param client HTTP Client
     * @param cr content resolver
     * @return path to uploaded photo on Parse.com
     * @throws IOException
     */
    public static String uploadPhoto(String path, HttpClient client, ContentResolver cr) throws IOException {
        String photoName = path == null ? "" : Support.getPhotoName(path);
        //creating post request to upload new photo of debtor if it is necessary
        HttpPost postRequest = new HttpPost(Constants.URI_FILE_PATHS + photoName);
        postRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
        postRequest.setHeader(Constants.REST_API_KEY_NAME,Constants.REST_API_KEY);
        //creating photo
        Bitmap photo = Support.getBitmap(path, cr, false);
        //if creating was successful - create new byte array entity
        ByteArrayEntity byteArrayEntity = null;
        if (photo != null){
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG,100,bos);
            byte[] bytePhotoArray = bos.toByteArray();
            byteArrayEntity = new ByteArrayEntity(bytePhotoArray);
        }
        // Uploading photo
        String pathOnParse = photo == null ? "" : Support.uploadFile(postRequest, client, byteArrayEntity);
        return pathOnParse;
    }


    /////////////////////////////////////////////////////////
    ////////////////////Contacts methods/////////////////////
    /////////////////////////////////////////////////////////

    /**
     * Getting contacts photo uri
     * @param uriContact uri of contact
     * @return photo uri
     */
    public static Uri getPhotoUri(Uri uriContact, ContentResolver cr) {
        String contactID = uriContact.getLastPathSegment();
        Uri contactPhotoUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactID));
        Uri photoUri = Uri.withAppendedPath(contactPhotoUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        return photoUri;
    }

    /**
     * Getting first or last name of contact
     * @param uriContact uri of contact
     * @param field field to retrieve
     * @return name
     */
    public static String getName(Uri uriContact,String field,ContentResolver cr){
        String contactID = uriContact.getLastPathSegment();
        String PROJECTION[] = {ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID,
                               field};
        String SELECTION = ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " = ?";
        String SELECTION_ARGS[] = {ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, contactID};
        String name = null;
        Cursor cursorData = cr.query(ContactsContract.Data.CONTENT_URI,
                PROJECTION,
                SELECTION,
                SELECTION_ARGS,
                null);
        if (cursorData != null){
            if (cursorData.moveToFirst()){
                name = cursorData.getString(cursorData.getColumnIndex(field));
            }
            cursorData.close();
        }
        return name;
    }

    /**
     * Getting contacts email
     * @param uriContact uri of contact
     * @return email
     */
    public static String getEmail(Uri uriContact, ContentResolver cr){
        String contactID = uriContact.getLastPathSegment();
        String email = null;
        Cursor cursorData = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactID,
                null,
                null);
        if (cursorData != null){
            if (cursorData.moveToFirst()){
                email = cursorData.getString(cursorData.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            }
            cursorData.close();
        }
        return email;
    }

    /**
     * Getting contacts field (for example phone number)
     * @param uriContact uri of contact
     * @param contentUri content uri
     * @param field field to retrieve
     * @param id id of field
     * @return field
     */
    public static String getContactField(Uri uriContact,Uri contentUri, String field, String id, ContentResolver cr){
        String contactField = null;
        String contactID = uriContact.getLastPathSegment();

        Cursor cursorData = cr.query(contentUri,
                null,
                id +  "=?" ,
                new String[] { contactID },
                null);
        if (cursorData != null){
            if (cursorData.moveToFirst()){
                contactField = cursorData.getString(cursorData.getColumnIndex(field));
            }
            cursorData.close();
        }
        return contactField;
    }
}
