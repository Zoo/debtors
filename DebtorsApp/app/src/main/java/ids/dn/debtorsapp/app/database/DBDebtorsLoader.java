package ids.dn.debtorsapp.app.database;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import ids.dn.debtorsapp.app.support.Constants;

/**
 * Created by dev4 on 28.02.14.
 */
public class DBDebtorsLoader extends CursorLoader {

    String userObjectId;

    public DBDebtorsLoader(Context context, String userObjectId) {
        super(context);
        this.userObjectId = userObjectId;
    }

    @Override
    public Cursor loadInBackground() {
        String query = "SELECT * FROM " + Constants.DB_TABLE_DEBTORS +
                " WHERE " + Constants.COLUMN_USER + " = " + "'" + userObjectId + "'";
        return DBDebtors.getInstance().getDataByRawQuery(query);
    }
}
