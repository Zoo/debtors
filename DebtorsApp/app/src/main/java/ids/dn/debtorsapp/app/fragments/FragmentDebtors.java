package ids.dn.debtorsapp.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.widget.Toast;

import ids.dn.debtorsapp.app.activities.DebtorsListActivity;
import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.database.DBDebtorsLoader;
import ids.dn.debtorsapp.app.listeners.TaskListener;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.R;
import ids.dn.debtorsapp.app.support.Support;
import ids.dn.debtorsapp.app.tasks.GetRecordTask;

/**
 * Created by dev4 on 28.02.14.
 */
public class FragmentDebtors extends FragmentBase implements LoaderManager.LoaderCallbacks<Cursor>,TaskListener {

    String userObjectId;
    ImageLoader imageLoader = ImageLoader.getInstance();
    BroadcastReceiver broadcastReceiver;
    ListView listView;

    public void setUserObjectId(String userObjectId) {
        this.userObjectId = userObjectId;
    }

    DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.ic_launcher)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .cacheOnDisc(true)
            .cacheInMemory(true)
            .showImageOnLoading(R.drawable.load)
            .showImageForEmptyUri(R.drawable.no_image)
            .build();
    DebtorsCursorAdapter debtorsCursorAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        DBDebtors.getInstance().open();
        View v = inflater.inflate(R.layout.fragment_list, null);
        getLoaderManager().initLoader(Constants.LOADER_ID_DEBTORS, null, this);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (Constants.ACTION_RESULT_PHOTO_UPLOADED.equals(action)){
                    Toast.makeText(getActivity(), "Debtor uploaded", Toast.LENGTH_SHORT).show();
                }
                if (Constants.ACTION_RESULT_DEBTOR_DELETED.equals(action)){
                    Toast.makeText(getActivity(), "Debtor deleted", Toast.LENGTH_SHORT).show();
                }
                if (Constants.ACTION_RESULT_DEBTOR_UPDATED.equals(action)){
                    Toast.makeText(getActivity(), "Debtor updated", Toast.LENGTH_SHORT).show();
                }
                if (Constants.ACTION_RESULT_SYNCHRONIZE_COMPLETED.equals(action)){
                    Toast.makeText(getActivity(), "Synchronized", Toast.LENGTH_SHORT).show();
                }
                if (Constants.ACTION_RESULT_SERVICE_CONNECTED.equals(action)){
                    //=================================================
                    //                  Synchronization
                    //=================================================
                    debtorsHttpService.synchronizeData(userObjectId);
                }
                getLoaderManager().getLoader(Constants.LOADER_ID_DEBTORS).forceLoad();
            }
        };

        listView = (ListView) v.findViewById(R.id.listView);
        debtorsCursorAdapter = new DebtorsCursorAdapter(getActivity(), null,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER, inflater);
        listView.setAdapter(debtorsCursorAdapter);
        /**
         * on click on the item getting id and objectId of the debtor
         * and querying for his debts
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.PARAM_TABLE_NAME, Constants.DB_TABLE_DEBTORS);
                bundle.putLong(Constants.PARAM_ID, id);
                new GetObjectIdTask(FragmentDebtors.this).execute(bundle);
            }
        });
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DBDebtors.getInstance().close();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        registerForContextMenu(listView);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("Debtors");
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        IntentFilter intentFilter =  new IntentFilter(Constants.ACTION_RESULT_PHOTO_UPLOADED);
        intentFilter.addAction(Constants.ACTION_RESULT_DEBTOR_DELETED);
        intentFilter.addAction(Constants.ACTION_RESULT_DEBTOR_UPDATED);
        intentFilter.addAction(Constants.ACTION_RESULT_DEBTOR_ADDED);
        intentFilter.addAction(Constants.ACTION_RESULT_SYNCHRONIZE_COMPLETED);
        intentFilter.addAction(Constants.ACTION_RESULT_SERVICE_CONNECTED);
        intentFilter.addAction(Constants.ACTION_RESULT_DELETED_FROM_DB);
        intentFilter.addAction(Constants.ACTION_RESULT_DB_UPDATED);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new DBDebtorsLoader(getActivity(),userObjectId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        debtorsCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        debtorsCursorAdapter.swapCursor(null);
    }

    private void openContactDialog(){
        AlertDialog.Builder newDebtorDialog = new AlertDialog.Builder(getActivity());
        newDebtorDialog.setTitle("Choose add operation");
        newDebtorDialog.setPositiveButton("Take contact", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent pickContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(pickContact, Constants.REQUEST_CODE_PICK_CONTACT);
            }
        });
        newDebtorDialog.setNegativeButton("Create new", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FragmentAddDebtor fragmentAddDebtor = new FragmentAddDebtor();
                fragmentAddDebtor.setUserObjectId(userObjectId);
                fragmentListener.replaceFragment(fragmentAddDebtor);
            }
        });
        newDebtorDialog.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, Constants.ITEM_DELETE, 0, "Delete");
        menu.add(0, Constants.ITEM_EDIT, 0, "Edit");
        menu.add(0, Constants.ITEM_BACK, 0, "Back");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_add_debtor_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_add_debtor :
                openContactDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo;
        ContentValues cv;
        switch (item.getItemId()){
            case Constants.ITEM_DELETE:
                adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                cv = new ContentValues();
                cv.put(Constants.PARAM_TABLE_NAME, Constants.DB_TABLE_DEBTORS);
                cv.put(Constants.PARAM_URI,Constants.URI_CLASS_DEBTOR);
                cv.put(Constants.PARAM_ID, adapterContextMenuInfo.id);
                debtorsHttpService.delete(cv);
                break;
            case Constants.ITEM_EDIT :
                adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.PARAM_TABLE_NAME, Constants.DB_TABLE_DEBTORS);
                bundle.putLong(Constants.PARAM_ID, adapterContextMenuInfo.id);
                /**
                 * Calling AsyncTask GetRecordTask. This task asynchronously getting record data
                 * by its ID and calling fireTaskComplete event. This fragment implements TaskListener
                 * so next operations performing in 'fireTaskComplete' function which described below
                 */
                new GetRecordTask(this).execute(bundle);
                break;
            case Constants.ITEM_BACK :
                getActivity().closeContextMenu();
                break;
        }
        return super.onContextItemSelected(item);
    }

    /**
     * Getting data from selected contact
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == Constants.REQUEST_CODE_PICK_CONTACT){
                    /**
                     * Creating AddDebtor Fragment with contact fields
                     */
                    Uri contactUri = data.getData();
                    ContentResolver cr = getActivity().getContentResolver();
                    String name = Support.getName(contactUri, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, cr);
                    String surname = Support.getName(contactUri, ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, cr);
                    String phoneNumber = Support.getContactField(contactUri, Phone.CONTENT_URI, Phone.NUMBER, Phone.CONTACT_ID, cr);
                    String email = Support.getEmail(contactUri, cr);
                    String photo = Support.getPhotoUri(contactUri, cr).toString();
                    FragmentAddDebtor fragmentAddDebtor = new FragmentAddDebtor();
                    fragmentAddDebtor.setParameters(0,name, surname ,email,phoneNumber,photo,null, "",userObjectId);
                    fragmentListener.replaceFragment(fragmentAddDebtor);
            }
        }
    }

    /**
     * Editing existing debtor
     * @param cursor cursor with data about chosen debtor
     */
    @Override
    public void fireTaskComplete(Cursor cursor) {
        if (cursor != null){
            if (cursor.moveToFirst()){
                /**
                 * getting data from record by it's cursor
                 */
                long id = cursor.getLong(cursor.getColumnIndex(Constants.COLUMN_ID));
                String objectId = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_OBJECT_ID));
                String name = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_NAME));
                String surname = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_SURNAME));
                String email = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_EMAIL));
                String phoneNumber = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_TELEPHONE_NUMBER));
                String pathOnParse = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_PHOTO_PARSE_PATH));
                String photo=cursor.getString(cursor.getColumnIndex(Constants.COLUMN_PHOTO));
                /**
                 * creating fragment to change selected debtors data
                 */
                FragmentAddDebtor fragmentAddDebtor = new FragmentAddDebtor();
                fragmentAddDebtor.setParameters(id,name,surname,email,phoneNumber,photo,pathOnParse,objectId,userObjectId);
                fragmentListener.replaceFragment(fragmentAddDebtor);
            }
            cursor.close();
        }
    }

    @Override
    public void returnString(String objectId) {
        FragmentDebts fragmentDebts = new FragmentDebts();
        fragmentDebts.setDebtorsObjectId(objectId);
        fragmentListener.replaceFragment(fragmentDebts);
    }

    class GetObjectIdTask extends AsyncTask<Bundle, Void, String>{

        TaskListener taskListener;

        GetObjectIdTask(TaskListener taskListener) {
            this.taskListener = taskListener;
        }

        @Override
        protected String doInBackground(Bundle... params) {
            String tableName = params[0].getString(Constants.PARAM_TABLE_NAME);
            long id = params[0].getLong(Constants.PARAM_ID);
            Cursor cursor = DBDebtors.getInstance().getRecord(tableName, id);
            String objectId = null;
            if (cursor != null){
                if(cursor.moveToFirst()){
                    objectId = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_OBJECT_ID));
                }
                cursor.close();
                DBDebtors.getInstance().close();
            }
            return objectId;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            taskListener.returnString(s);
        }
    }

    class DebtorsCursorAdapter extends CursorAdapter {

        LayoutInflater layoutInflater;

        public DebtorsCursorAdapter(Context context, Cursor c,int flags, LayoutInflater layoutInflater) {
            super(context, c, flags);
            this.layoutInflater = layoutInflater;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = layoutInflater.inflate(R.layout.item_debtor, null);
            bindView(view,context,cursor);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            if (cursor != null){
                ImageView debtorPhoto = (ImageView) view.findViewById(R.id.ivDebtorPhoto);
                TextView tvName = (TextView) view.findViewById(R.id.tvName);
                TextView tvSurname = (TextView) view.findViewById(R.id.tvSurname);
                TextView tvEmail = (TextView) view.findViewById(R.id.tvEmail);

                tvName.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_NAME)));
                tvSurname.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_SURNAME)));
                tvEmail.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_EMAIL)));
                String pathOnParse = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_PHOTO_PARSE_PATH));
                if (TextUtils.isEmpty(pathOnParse)){
                    String localPath = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_PHOTO));
                    Bitmap bitmap = Support.getBitmap(localPath,getActivity().getContentResolver(), true);
                    if (bitmap == null){
                        debtorPhoto.setImageResource(R.drawable.no_image);
                    } else {
                        debtorPhoto.setImageBitmap(bitmap);
                    }
                } else {
                    imageLoader.displayImage(pathOnParse,debtorPhoto,options);
                }
            }
        }
    }
}