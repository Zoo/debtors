package ids.dn.debtorsapp.app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.widget.LoginButton;
import com.facebook.widget.UserSettingsFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.plus.PlusClient;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.gson.Gson;
import com.facebook.*;
import com.facebook.model.*;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCaptchaDialog;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ids.dn.debtorsapp.app.R;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.GSONClasses.User;
import ids.dn.debtorsapp.app.support.Support;
import ids.dn.debtorsapp.app.tasks.PostRequestAsyncTask;

/**
 * A login screen that offers login via email/password and via Google+ sign in.
 * <p/>
 * ************ IMPORTANT SETUP NOTES: ************
 * In order for Google+ sign in to work with your app, you must first go to:
 * https://developers.google.com/+/mobile/android/getting-started#step_1_enable_the_google_api
 * and follow the steps in "Step 1" to create an OAuth 2.0 client for your package.
 */
public class LoginActivity extends PlusBaseActivity implements LoaderCallbacks<Cursor>,GooglePlayServicesClient.ConnectionCallbacks{

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mEmailLoginFormView;
    private View mSignOutButtons;
    private View mLoginFormView;
    private SignInButton mPlusSignInButton;

    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback facebookCallback;
    LoginButton facebookLoginButton;
    CheckBox cbRememberMe;
    ProgressBar progressBar;
    SharedPreferences sharedPreferences;
    ImageView ivVkAuth;
    PlusClient mPlusClient;
    ImageView ivLinkedId;

    String FACEBOOK = "facebook_";
    String VK = "vk_";
    String VK_PASS = "vk_pass_";
    String GMAIL_PASS = "gmail_pass_";
    String GMAIL = "gmail_";
    String LINKEDIN = "linkedin_";
    String LINKEDIN_PASS = "linkedin_pass_";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("Remember your debtors !");

        VKUIHelper.onCreate(this);
        vkInitialization();

        facebookInitialization();
        uiHelper.onCreate(savedInstanceState);

        mPlusClient = getPlusClient();
        //Set a custom web view client


        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        cbRememberMe = (CheckBox) findViewById(R.id.cbRememberMe);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mEmailLoginFormView = findViewById(R.id.email_login_form);
//        mSignOutButtons = findViewById(R.id.plus_sign_out_buttons);

        progressBar = new ProgressBar(getApplicationContext());
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        sharedPreferences = this.getSharedPreferences("pref",MODE_PRIVATE);
        String login = sharedPreferences.getString(Constants.LOGIN, "");
        String password = sharedPreferences.getString(Constants.PASSWORD, "");
        progressBar.setVisibility(View.INVISIBLE);
        cbRememberMe.setChecked(false);
        if (!"".equals(login) && !"".equals(password)){
            mEmailSignInButton.setEnabled(false);
            mEmailView.setText(login);
            mPasswordView.setText(password);
            cbRememberMe.setChecked(true);
            new UserLoginTask(login,password).execute();
        } else {
            // logouts

            // facebook
            Session.getActiveSession().closeAndClearTokenInformation();
            // vk
            VKSdk.logout();
            // google+
            revokeAccess();
            // LinkedIn
//            LinkedInLogout();
        }
        // Find the Google+ sign in button.
        mPlusSignInButton = (SignInButton) findViewById(R.id.plus_sign_in_button);
        mPlusSignInButton.setSize(SignInButton.SIZE_ICON_ONLY);
        if (supportsGooglePlayServices()) {
            // Set a listener to connect the user when the G+ button is clicked.
            mPlusSignInButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    signIn();
                }
            });
        } else {
            // Don't offer G+ sign in if the app's version is too low to support Google Play
            // Services.
            mPlusSignInButton.setVisibility(View.GONE);
            return;
        }

        ivLinkedId = (ImageView) findViewById(R.id.ivLinkedIn);
        ivLinkedId.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get the authorization Url
                Intent intent = new Intent(LoginActivity.this, LinkedInActivity.class);
                startActivityForResult(intent, Constants.REQUEST_CODE_GET_EMAIL);
            }
        });
    }

    private void LinkedInLogout(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                HttpClient client = new DefaultHttpClient();
                HttpGet logout = new HttpGet("https://api.linkedin.com/uas/oauth/invalidateToken");
                try {
                    client.execute(logout);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        // ======================================================
        //                       LinkedIn
        // ======================================================
        if (responseCode == RESULT_OK){
            if (requestCode == Constants.REQUEST_CODE_GET_EMAIL){
                String email = intent.getStringExtra(Constants.PARAM_DATA);
                new UserLoginTask(LINKEDIN + email, LINKEDIN_PASS + email).execute();
            }
        }
        uiHelper.onActivityResult(requestCode, responseCode, intent);
        VKUIHelper.onActivityResult(requestCode, responseCode, intent);
    }

    private void populateAutoComplete() {
        if (VERSION.SDK_INT >= 14) {
            // Use ContactsContract.Profile (API 14+)
//            getLoaderManager().initLoader(0,null,this);
        } else if (VERSION.SDK_INT >= 8) {
            // Use AccountManager (API 8+)
            new SetupEmailAutoCompleteTask().execute(null, null);
        }
    }


    private void facebookInitialization(){
        // ======================================================
        //               facebook initialization
        // ======================================================
        facebookLoginButton = (LoginButton) findViewById(R.id.facebookLoginButton);
        facebookLoginButton.setReadPermissions(Arrays.asList("email"));
        facebookCallback = new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if(session.isOpened()){
                    Request.newMeRequest(session, new Request.GraphUserCallback() {
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            if (user != null){
                                String id = user.getId();
                                JSONObject userInfo = user.getInnerJSONObject();
                                String email = null;
                                try {
                                    email = userInfo.getString("email");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                new UserLoginTask(email, FACEBOOK + id).execute();
                            }
                        }
                    }).executeAsync();
                }
            }
        };
        uiHelper = new UiLifecycleHelper(this, facebookCallback);
    }

    private void vkInitialization(){
        // ======================================================
        //                  VK initialization
        // ======================================================
        VKSdk.initialize(new VKSdkListener() {
            @Override
            public void onCaptchaError(VKError captchaError) {
                new VKCaptchaDialog(captchaError).show();
            }

            @Override
            public void onTokenExpired(VKAccessToken expiredToken) {
                VKSdk.authorize(null);
            }

            @Override
            public void onAccessDenied(VKError authorizationError) {
                new AlertDialog.Builder(LoginActivity.this)
                        .setMessage(authorizationError.errorMessage)
                        .show();
            }
            @Override
            public void onReceiveNewToken(VKAccessToken newToken) {
                new UserLoginTask(VK + newToken.userId, VK_PASS + newToken.userId).execute();
            }

            @Override
            public void onAcceptUserToken(VKAccessToken token) {
                new UserLoginTask(VK + token.userId, VK_PASS + token.userId).execute();
            }
        }, getResources().getString(R.string.vk_app_id));

        ivVkAuth = (ImageView) findViewById(R.id.ivVKAuth);
        ivVkAuth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                VKSdk.authorize(null, true, true);
            }
        });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            progressBar.setVisibility(View.INVISIBLE);
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            progressBar.setVisibility(View.VISIBLE);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    private boolean isPasswordValid(String password) {
        if (TextUtils.isEmpty(password)){
            return  false;
        }
        if (password.length() < 4){
            return false;
        }
        return true;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onPlusClientSignIn() {
        //Set up sign out and disconnect buttons.
//        Button signOutButton = (Button) findViewById(R.id.plus_sign_out_button);
//        signOutButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                signOut();
//            }
//        });
//        Button disconnectButton = (Button) findViewById(R.id.plus_disconnect_button);
//        disconnectButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                revokeAccess();
//            }
//        });
        String account = mPlusClient.getAccountName();
        new UserLoginTask(GMAIL + account, GMAIL_PASS + account).execute();
    }

    @Override
    protected void onPlusClientBlockingUI(boolean show) {
//        showProgress(show);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void updateConnectButtonState() {
        //TODO: Update this logic to also handle the user logged in by email.
        boolean connected = getPlusClient().isConnected();

//        mSignOutButtons.setVisibility(connected ? View.VISIBLE : View.GONE);
//        mEmailLoginFormView.setVisibility(connected ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onPlusClientRevokeAccess() {
        // TODO: Access to the user's G+ account has been revoked.  Per the developer terms, delete
        // any stored user data here.
    }

    @Override
    protected void onPlusClientSignOut() {

    }

    /**
     * Check if the device supports Google Play Services.  It's best
     * practice to check first rather than handling this as an error case.
     *
     * @return whether the device supports Google Play Services
     */
    private boolean supportsGooglePlayServices() {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) ==
                ConnectionResult.SUCCESS;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                                                                     .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<String>();
        if (cursor != null){
            if (cursor.moveToFirst()){
                while (!cursor.isAfterLast()) {
                    emails.add(cursor.getString(ProfileQuery.ADDRESS));
                    cursor.moveToNext();
                }
            }
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Use an AsyncTask to fetch the user's email addresses on a background thread, and update
     * the email text field with results on the main UI thread.
     */
    class SetupEmailAutoCompleteTask extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected List<String> doInBackground(Void... voids) {
            ArrayList<String> emailAddressCollection = new ArrayList<String>();

            // Get all emails from the user's contacts and copy them to a list.
            ContentResolver cr = getContentResolver();
            Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                    null, null, null);
            while (emailCur.moveToNext()) {
                String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract
                        .CommonDataKinds.Email.DATA));
                emailAddressCollection.add(email);
            }
            emailCur.close();

            return emailAddressCollection;
        }

	    @Override
	    protected void onPostExecute(List<String> emailAddressCollection) {
	       addEmailsToAutoComplete(emailAddressCollection);
	    }
    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, String> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            mEmailView.setEnabled(false);
            mPasswordView.setEnabled(false);
        }

        @Override
        protected String doInBackground(Void... params) {

            String objectId = null;
            HttpClient client = new DefaultHttpClient();
            HttpGet loginRequest = new HttpGet(new Uri.Builder()
                    .scheme("https")
                    .authority("api.parse.com")
                    .path("/1/login")
                    .appendQueryParameter("username", mEmail)
                    .appendQueryParameter("password", mPassword)
                    .build().toString());

            loginRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
            loginRequest.setHeader(Constants.REST_API_KEY_NAME,Constants.REST_API_KEY);
            if (!Support.checkConnection(getApplicationContext())){
                Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
                return null;
            }
            try {
                HttpResponse response = client.execute(loginRequest);
                // 200 - response OK
                if (response.getStatusLine().getStatusCode() == 200){
                    JSONObject responseJSONObject = Support.parseToJSONObject(response.getEntity().getContent());
                    objectId = responseJSONObject.getString("objectId");
                }
                if (objectId == null){
                    //registration
                    HttpPost signInRequest = new HttpPost(Constants.URI_SIGNIN);
                    signInRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
                    signInRequest.setHeader(Constants.REST_API_KEY_NAME,Constants.REST_API_KEY);

                    Gson gson = new Gson();
                    User user = new User(mEmail,mPassword);
                    String json = gson.toJson(user);
                    objectId = Support.createNewUser(json, signInRequest, client);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String login = mEmail;
            String password = "";
            if (cbRememberMe.isChecked()){
                password = mPassword;
            }
            SharedPreferences.Editor ed = sharedPreferences.edit();
            ed.putString(Constants.LOGIN, login);
            ed.putString(Constants.PASSWORD, password);
            ed.commit();
            return objectId;
        }

        @Override
        protected void onPostExecute(final String userObjectId) {
            mAuthTask = null;
            progressBar.setVisibility(View.VISIBLE);
            mEmailView.setEnabled(true);
            mPasswordView.setEnabled(true);
            if (userObjectId != null) {
                finish();
                Intent debtorsActivityIntent = new Intent(LoginActivity.this, DebtorsListActivity.class);
                debtorsActivityIntent.putExtra(Constants.PARAM_DATA, userObjectId);
                startActivity(debtorsActivityIntent);
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            progressBar.setVisibility(View.INVISIBLE);
            mEmailView.setEnabled(true);
            mPasswordView.setEnabled(true);
        }
    }
}



