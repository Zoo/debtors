package ids.dn.debtorsapp.app.tasks;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import ids.dn.debtorsapp.app.activities.DebtorsListActivity;
import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.GSONClasses.Pointer;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 13.03.14.
 */
public class SynchronizeTask extends AsyncTask<String, Void, Boolean> {

    Context context;

    public SynchronizeTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Intent begin = new Intent(Constants.ACTION_RESULT_SYNCHRONIZE_BEGINS);
        context.sendBroadcast(begin);
    }

    @Override
    protected Boolean doInBackground(String... params) {
        //Check is DB empty
        Boolean isSynchronized = false;
        Cursor cursor = DBDebtors.getInstance().getAllDataFromTable(Constants.DB_TABLE_DEBTORS);
        if (cursor != null){
            if(cursor.moveToFirst()){
                // DB is not empty, don't need to synchronize
                cursor.close();
                return false;
            }
            cursor.close();
        }
        String usersObjectId = params[0];
        try {
            synchronizeDebtors(usersObjectId);
            isSynchronized = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isSynchronized;
    }

    @Override
    protected void onPostExecute(Boolean isSynchronized) {
        super.onPostExecute(isSynchronized);
        if (isSynchronized){
            Intent synchronizeCompleteIntent = new Intent(Constants.ACTION_RESULT_SYNCHRONIZE_COMPLETED);
            context.sendBroadcast(synchronizeCompleteIntent);
        } else {
            Intent canceled = new Intent(Constants.ACTION_RESULT_SYNCHRONIZE_CANCELED);
            context.sendBroadcast(canceled);
        }
    }

    /**
     * Synchronize debtor to local database
     * @param usersObjectId users objectId
     * @throws JSONException parsing response json object
     * @throws IOException
     */
    private void synchronizeDebtors(String usersObjectId) throws JSONException, IOException {
        // Creating HTTPGet request and set it's parameters
        HttpClient client = new DefaultHttpClient();
        HttpGet getAllDataRequest = new HttpGet();
        getAllDataRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
        getAllDataRequest.setHeader(Constants.REST_API_KEY_NAME, Constants.REST_API_KEY);

        // Creating request parameters
        Map<String,String> debtorRequestParams = new HashMap<String, String>();
        debtorRequestParams.put(Constants.COLUMN_USER, usersObjectId);
        JSONObject jsonDebtor = new JSONObject(debtorRequestParams);
        String uriDebtors = new Uri.Builder()
                .scheme("https")
                .authority("api.parse.com")
                .path("/1/classes/Debtor")
                .appendQueryParameter("where", jsonDebtor.toString())
                .build().toString();
        getAllDataRequest.setURI(URI.create(uriDebtors));
        // Get result from GET request
        HttpResponse responseDebtor = client.execute(getAllDataRequest);
        // Get JSONObject
        JSONObject resultDebtors = Support.parseToJSONObject(responseDebtor.getEntity().getContent());
        //Get JSONArray from JSONObject. Array contains information about every debtor
        JSONArray arrayResultsDebtors = resultDebtors.getJSONArray("results");
        for (int debtorsIndex = 0; debtorsIndex < arrayResultsDebtors.length(); debtorsIndex++){
            JSONObject debtor = arrayResultsDebtors.getJSONObject(debtorsIndex);
            // Creating content values and filling it
            ContentValues cvDebtors = new ContentValues();
            // debtorsObjectId is needed to request debtor's debts
            String debtorsObjectId = debtor.getString(Constants.COLUMN_OBJECT_ID);
            cvDebtors.put(Constants.COLUMN_OBJECT_ID, debtor.getString(Constants.COLUMN_OBJECT_ID));
            cvDebtors.put(Constants.COLUMN_NAME, debtor.getString(Constants.COLUMN_NAME));
            cvDebtors.put(Constants.COLUMN_SURNAME, debtor.getString(Constants.COLUMN_SURNAME));
            cvDebtors.put(Constants.COLUMN_TELEPHONE_NUMBER, debtor.getString(Constants.COLUMN_TELEPHONE_NUMBER));
            cvDebtors.put(Constants.COLUMN_EMAIL, debtor.getString(Constants.COLUMN_EMAIL));
            cvDebtors.put(Constants.COLUMN_USER, usersObjectId);
            //Издержки хранения на парсе (там локальный путь не хранится, а поле с путем на парсе называется photo)
            String pathOnParse = debtor.getString(Constants.COLUMN_PHOTO);
            cvDebtors.put(Constants.COLUMN_PHOTO_PARSE_PATH, pathOnParse);
            cvDebtors.put(Constants.COLUMN_PHOTO, downloadImage(pathOnParse));
            // Write to DB
            DBDebtors.getInstance().addRecord(Constants.DB_TABLE_DEBTORS, cvDebtors);
            // Filling debts of this debtor
            synchronizeDebts(debtorsObjectId);
        }
    }

    /**
     * Synchronizing debts to database from Parse.com
     * @param debtorsObjectId debtors objectId
     * @throws IOException
     * @throws JSONException
     */
    private void synchronizeDebts(String debtorsObjectId) throws IOException, JSONException {
        HttpClient client = new DefaultHttpClient();

        Gson gson = new Gson();
        // Creating Map with parameters for request
        Map<String,Pointer> debtRequestParams = new HashMap<String, Pointer>();
        /**
         * Requests with WHERE clause using Pointer type on Parse.com have special format
         * "where = {field = Pointer}". In our case  field = "debtor" and Pointer have next format
         * Pointer = {"__type" : "Pointer",  "classname" : "Debtor", "objectId" : "debtorsObjectId"}
         */
        Pointer pointerDebtor = new Pointer("Pointer","Debtor",debtorsObjectId);
        debtRequestParams.put(Constants.COLUMN_POINTER_DEBTOR, pointerDebtor);
        // Parsing Map with parameters to JSON format
        String jsonDebt = gson.toJson(debtRequestParams);
        // Creating uri using parameters created above
        String uriDebts = new Uri.Builder()
                .scheme("https")
                .authority("api.parse.com")
                .path("/1/classes/Debt")
                .appendQueryParameter("where", jsonDebt)
                .build().toString();
        // Creating HTTPGet request and set it's parameters
        HttpGet getDebtsRequest = new HttpGet(uriDebts);
        getDebtsRequest.setHeader(Constants.APPLICATION_ID_NAME,Constants.APPLICATION_ID);
        getDebtsRequest.setHeader(Constants.REST_API_KEY_NAME, Constants.REST_API_KEY);
        HttpResponse responseDebt = client.execute(getDebtsRequest);
        /**
         * Getting result from response. Result represents as JSONObject that contains
         * JSONArray with name "results", every element of this array is also JSONObject
         * contains information about single debt in our case
         */
        // Getting result
        JSONObject resultDebts = Support.parseToJSONObject(responseDebt.getEntity().getContent());
        // Getting array of Debts
        JSONArray arrayResultsDebts = resultDebts.getJSONArray("results");
        for (int debtIndex = 0; debtIndex < arrayResultsDebts.length(); debtIndex++){
            JSONObject debt = arrayResultsDebts.getJSONObject(debtIndex);
            ContentValues cvDebts = new ContentValues();
            // Creating content values for database and filling it
            int isDebtMoney = debt.getInt(Constants.COLUMN_IS_DEBT_MONEY);
            cvDebts.put(Constants.COLUMN_IS_DEBT_MONEY, isDebtMoney);
            cvDebts.put(Constants.COLUMN_OBJECT_ID, debt.getString(Constants.COLUMN_OBJECT_ID));
            cvDebts.put(Constants.COLUMN_DATE_LOAN,debt.getString(Constants.COLUMN_DATE_LOAN));
            cvDebts.put(Constants.COLUMN_DATE_RETURN,debt.getString(Constants.COLUMN_DATE_RETURN));
            JSONObject debtorPointer = new JSONObject(debt.getString(Constants.COLUMN_POINTER_DEBTOR));
            cvDebts.put(Constants.COLUMN_POINTER_DEBTOR, debtorPointer.getString(Constants.COLUMN_OBJECT_ID));
            cvDebts.put(Constants.COLUMN_REMIND_EVENT_ID, debt.getLong(Constants.COLUMN_REMIND_EVENT_ID));
            switch (isDebtMoney){
                case 0 :
                    cvDebts.put(Constants.COLUMN_NAME,debt.getString(Constants.COLUMN_NAME));
                    String photoParse = debt.getString(Constants.COLUMN_PHOTO_ON_PARSE);
                    cvDebts.put(Constants.COLUMN_PHOTO_PARSE_PATH, photoParse);
                    cvDebts.put(Constants.COLUMN_PHOTO, downloadImage(photoParse));
                    break;
                case 1:
                    cvDebts.put(Constants.COLUMN_AMOUNT, debt.getString(Constants.COLUMN_AMOUNT));
                    break;
            }
            // Adding record to database
            DBDebtors.getInstance().addRecord(Constants.DB_TABLE_DEBTS, cvDebts);
        }
    }

    /**
     * Downloading image from Parse.com to 'DebtorsAppImages' folder
     * and return local path to it
     * @param pathOnParse path to photo on Parse.com
     * @return local path to photo
     */
    private String downloadImage(String pathOnParse){
        ImageLoader imageLoader = ImageLoader.getInstance();
        Bitmap photoLocalStorage = imageLoader.loadImageSync(pathOnParse);
        String[] photoNameArray = pathOnParse.split("\\-");
        String photoName = photoNameArray[photoNameArray.length - 1];
        if (!TextUtils.isEmpty(photoName)){
            File photo = new File(DebtorsListActivity.photoDirectoryPath + File.separator + photoName);
            try {
                FileOutputStream fos = new FileOutputStream(photo);
                photoLocalStorage.compress(Bitmap.CompressFormat.JPEG,100,fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return photo.getAbsolutePath();
        }
        return "";
    }
}


