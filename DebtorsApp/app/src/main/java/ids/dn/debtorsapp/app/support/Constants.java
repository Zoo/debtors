package ids.dn.debtorsapp.app.support;

/**
 * Created by dev4 on 28.02.14.
 */
public class Constants {
    /**
     * Database
     */
    public static final String DB_NAME = "DebtorsDB";
    public static final int DB_VERSION = 1;
    public static final String DB_TABLE_DEBTORS = "Debtors";
    public static final String DB_TABLE_DEBTS = "Debt";
    public static final String DB_TABLE_USERS = "Users";
    // Parameters
    public static final String PARAM_TABLE_NAME = "table_name";
    public static final String PARAM_DATA = "data";
    public static final String PARAM_ID = "id";
    public static final String PARAM_URI = "uri";
    // Database columns
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_SURNAME = "surname";
    public static final String COLUMN_TELEPHONE_NUMBER = "telephone_number";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_DATE_LOAN = "date_loan";
    public static final String COLUMN_DATE_RETURN = "date_return";
    public static final String COLUMN_REMIND_EVENT_ID = "remind_event_id";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_PHOTO = "photo";
    public static final String COLUMN_IS_DEBT_MONEY = "is_debt_money";
    public static final String COLUMN_PHOTO_PARSE_PATH = "photo_parse_path";
    public static final String COLUMN_PHOTO_ON_PARSE = "photo_on_parse";
    public static final String COLUMN_POINTER_DEBTOR = "debtor";
    public static final String COLUMN_USER = "user";
    // Id on parse.com
    public static final String COLUMN_OBJECT_ID = "objectId";
    /**
     * Parse.com
     */
    // Upload uris
    public static final String URI_FILE_PATHS = "https://api.parse.com/1/files/";
    public static final String URI_CLASS_DEBTOR = "https://api.parse.com/1/classes/Debtor";
    public static final String URI_CLASS_DEBT = "https://api.parse.com/1/classes/Debt";
    public static final String URI_SIGNIN = "https://api.parse.com/1/users";
    public static final String URI_BATCH_DELETE = "https://api.parse.com/1/batch";
    //  Application keys
    public static final String APPLICATION_ID_NAME = "X-Parse-Application-Id";
    public static final String APPLICATION_ID = "dShOqYCPqp3xtGRB6OWSMHFAhldpOaauVksS04cd";
    public static final String REST_API_KEY_NAME = "X-Parse-REST-API-Key";
    public static final String REST_API_KEY = "FnOSWErzxQqbjoeCuwDztb9Q05MJCw0KpXKMrwTp";

    // Other
    public static final int LOADER_ID_DEBTORS = 888;
    public static final int LOADER_ID_DEBTS = 777;
    public static final int DEBT_TYPE_MONEY = 1;
    public static final int DEBT_TYPE_STUFF = 0;
    public static final String IMAGE_FOLDER_NAME = "DebtorsAppImages";
    public static final String LOGIN = "Login";
    public static final String PASSWORD = "Password";
    public static final String EXTENSION_JPG = ".jpg";
    public static final String METHOD_DELETE= "DELETE";

    public static final String ACTION_RESULT_PHOTO_UPLOADED = "action_result_photo_uploaded";
    public static final String ACTION_RESULT_DEBTOR_DELETED= "action_result_debtor_deleted";
    public static final String ACTION_RESULT_DEBTOR_ADDED= "action_result_debtor_added";
    public static final String ACTION_RESULT_DEBT_DELETED= "action_result_debt_deleted";
    public static final String ACTION_RESULT_DELETED_FROM_DB= "action_result_deleted_from_db";
    public static final String ACTION_RESULT_DEBT_ADDED= "action_result_debt_added";
    public static final String ACTION_RESULT_DB_UPDATED= "action_result_db_updated";
    public static final String ACTION_RESULT_DEBTOR_UPDATED= "action_result_debtor_updated";
    public static final String ACTION_RESULT_DEBT_UPDATED= "action_result_debt_updated";
    public static final String ACTION_RESULT_SYNCHRONIZE_COMPLETED = "action_result_synchronize_completed";
    public static final String ACTION_RESULT_SYNCHRONIZE_BEGINS = "action_result_synchronize_begins";
    public static final String ACTION_RESULT_SYNCHRONIZE_CANCELED = "action_result_synchronize_canceled";
    public static final String ACTION_RESULT_SERVICE_CONNECTED = "action_result_service_connected";
    public static final int REQUEST_CAMERA_PIC = 22;
    public static final int REQUEST_TAKE_PHOTO_FROM_SD = 33;
    public static final int REQUEST_CODE_PICK_CONTACT = 1;
    public static final int REQUEST_CODE_GET_EMAIL = 44;
    public static final int ITEM_EDIT = 1;
    public static final int ITEM_DELETE = 2;
    public static final int ITEM_BACK = 3;
}
