package ids.dn.debtorsapp.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ids.dn.debtorsapp.app.R;
import ids.dn.debtorsapp.app.activities.DebtorsListActivity;
import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 28.02.14.
 */
public class FragmentAddDebtor extends FragmentBase implements View.OnClickListener{

    EditText etName;
    EditText etSurname;
    EditText etPhone;
    EditText etEmail;
    ImageView ivPhoto;
    String pathToPhoto;

    boolean isPhotoChanged = false;
    boolean isEdit = false;
    boolean newContact = true;

    String userObjectId;

    long id;
    String objectId;
    String pathOnParse;
    String name = "";
    String surname = "";
    String email = "";
    String phoneNumber = "";
    String photo;

    public void setUserObjectId(String userObjectId) {
        this.userObjectId = userObjectId;
    }

    public void setParameters(long id, String name, String surname, String email, String phoneNumber,
                             String photo,String pathOnParse, String objectId, String userObjectId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.photo = photo;
        this.pathOnParse = pathOnParse;
        this.objectId = objectId;
        this.userObjectId = userObjectId;
        newContact = false;
        /**
         * If objectId exists - then contact is editing
         */
        if (!"".equals(objectId)){
            isEdit = true;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        DBDebtors.getInstance().open();
        View view = inflater.inflate(R.layout.fragment_add_debtor, null);
        Button bAddNewDebtor = (Button) view.findViewById(R.id.bAddNewDebtor);
        etName = (EditText) view.findViewById(R.id.etDebtorsName);
        etSurname = (EditText) view.findViewById(R.id.etDebtorsSurname);
        etPhone = (EditText) view.findViewById(R.id.etDebtorsPhone);
        etEmail = (EditText) view.findViewById(R.id.etDebtorsEmail);
        ivPhoto = (ImageView) view.findViewById(R.id.imageView);
        ivPhoto.setOnClickListener(this);
        bAddNewDebtor.setOnClickListener(this);

        etName.setText(name);
        etPhone.setText(phoneNumber);
        etEmail.setText(email);
        etSurname.setText(surname);

        if (isEdit){
            bAddNewDebtor.setText(R.string.button_update_text);
            ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_update_debtor);
        } else{
            ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_add_new_debtor);
        }
        // saving local path to photo
        if(pathToPhoto == null && photo != null){
            pathToPhoto = photo;
        }
        // if path to photo is not null - creating photo by this path and setting it to image view
        Bitmap bitmapPhoto = Support.getBitmap(pathToPhoto, getActivity().getContentResolver(), true);
        if (bitmapPhoto != null){
            ivPhoto.setImageBitmap(bitmapPhoto);
        } else {
            // if photo was not setted to image view - set default image
            ivPhoto.setImageResource(R.drawable.no_image);
        }

        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DBDebtors.getInstance().close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case Constants.REQUEST_CAMERA_PIC:
                    break;
                case Constants.REQUEST_TAKE_PHOTO_FROM_SD:
                    pathToPhoto = Support.getRealPathFromURI(data.getData(),getActivity().getContentResolver());
                    break;
            }
            ivPhoto.setImageBitmap(BitmapFactory.decodeFile(pathToPhoto));
            /**
             * if we here - it's means that photo was changed
             * so we changing corresponding flag and clearing
             * path on parse.com, because when we will upload
             * new photo - we will get new path to it
             */
            if (isEdit){
                isPhotoChanged = true;
                pathOnParse = "";
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageView :
                openPhotoDialog();
                break;
            case R.id.bAddNewDebtor :
                if (!checkValidity()){
                    return;
                }
                ContentValues cv = new ContentValues();
                String name = etName.getText().toString();
                String surname = etSurname.getText().toString();
                String phoneNumber = etPhone.getText().toString();
                String email = etEmail.getText().toString();

                cv.put(Constants.COLUMN_NAME, name);
                cv.put(Constants.COLUMN_SURNAME, surname);
                cv.put(Constants.COLUMN_TELEPHONE_NUMBER, phoneNumber);
                cv.put(Constants.COLUMN_EMAIL, email);
                cv.put(Constants.COLUMN_PHOTO,pathToPhoto);
                cv.put(Constants.COLUMN_USER,userObjectId);
                /**
                 * If it's not edit - uploading debtor
                 * else - updating existing
                 */
                if (!isPhotoChanged && !isEdit){
                    debtorsHttpService.uploadDebtor(cv);
                } else{
                    cv.put(Constants.COLUMN_PHOTO_PARSE_PATH,pathOnParse);
                    cv.put(Constants.COLUMN_OBJECT_ID,objectId);
                    cv.put(Constants.COLUMN_ID,id);
                    debtorsHttpService.updateDebtor(cv);
                }
                /**
                 * Creating new contact only if it's really new, not if it
                 * was picked from contacts
                 */
                if (newContact){
                    createContactDialog();
                }
                getActivity().onBackPressed();
                break;
        }
    }

    private void createContactDialog(){
        AlertDialog.Builder newDebtorDialog = new AlertDialog.Builder(getActivity());
        newDebtorDialog.setTitle(R.string.question_add_to_contact);
        final ContentResolver cr = getActivity().getContentResolver();
        newDebtorDialog.setPositiveButton(R.string.answer_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Creating bitmap and byte array of this bitmap to create new contact
                Bitmap photoBitmap = Support.getBitmap(pathToPhoto,cr, true);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                if (pathToPhoto != null){
                    photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                }
                byte[] photoByteArray= bos.toByteArray();
                // Creating new contact
                createNewContact(etName.getText().toString(),
                        etSurname.getText().toString(),
                        etPhone.getText().toString(),
                        etEmail.getText().toString(),
                        photoByteArray,
                        cr);
            }
        });
        newDebtorDialog.setNegativeButton(R.string.answer_no, null);
        newDebtorDialog.show();
    }

    /**
     * Creating new contact in contacts
     * @param name name
     * @param surname surname
     * @param phoneNumber telephone number
     * @param email email address
     * @param photo local path to photo
     * @param cr content resolver
     */
    private void createNewContact(String name, String surname, String phoneNumber, String email, byte[] photo, ContentResolver cr){
        ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
        operations.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());
        // Name
        operations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
                .withValue(StructuredName.FAMILY_NAME, surname)
                .withValue(StructuredName.GIVEN_NAME, name).build());
        // Phone
        operations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
                .withValue(Phone.NUMBER, phoneNumber)
                .withValue(Phone.TYPE, Phone.TYPE_MOBILE).build());
        // E-mail
        operations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE)
                .withValue(Email.DATA, email).build());
        // Photo
        operations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, Photo.CONTENT_ITEM_TYPE)
                .withValue(Photo.PHOTO, photo).build());
        try {
            cr.applyBatch(ContactsContract.AUTHORITY, operations);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    private void openPhotoDialog(){
        AlertDialog.Builder newDebtorDialog = new AlertDialog.Builder(getActivity());
        newDebtorDialog.setTitle("Choose operation");
        newDebtorDialog.setPositiveButton(R.string.photo_from_sd_card, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, Constants.REQUEST_TAKE_PHOTO_FROM_SD);
            }
        });
        newDebtorDialog.setNegativeButton(R.string.photo_from_camera, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String photoName = "Debtor" + timeStamp;
                File photo = new File(DebtorsListActivity.photoDirectoryPath + File.separator + photoName + ".jpg");
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                pathToPhoto = photo.getPath();
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                startActivityForResult(cameraIntent, Constants.REQUEST_CAMERA_PIC);
            }
        });
        newDebtorDialog.show();
    }

    private boolean checkValidity(){
        if (TextUtils.isEmpty(etName.getText().toString()) && TextUtils.isEmpty(etSurname.getText().toString()) ){
            Toast.makeText(getActivity(), R.string.error_empty_name_surname,Toast.LENGTH_LONG).show();
            return false;
        }
        if (!TextUtils.isEmpty(etEmail.getText().toString())){
            if (!isEmailValid(etEmail.getText().toString())){
                Toast.makeText(getActivity(),R.string.error_email,Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if (!TextUtils.isEmpty(etPhone.getText().toString())){
            if (!PhoneNumberUtils.isGlobalPhoneNumber(etPhone.getText().toString())){
                Toast.makeText(getActivity(),R.string.error_phone_number,Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    private boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
