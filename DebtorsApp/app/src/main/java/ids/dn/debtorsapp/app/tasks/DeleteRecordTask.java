package ids.dn.debtorsapp.app.tasks;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.support.GSONClasses.BatchDeletingObject;

/**
 * Created by dev4 on 28.02.14.
 */
public class DeleteRecordTask extends AsyncTask<Bundle,Bundle,String> {

    Context context;

    public DeleteRecordTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onProgressUpdate(Bundle... values) {
        super.onProgressUpdate(values);
        // delete record from database and send broadcast to notify fragment and reload list
        DBDebtors.getInstance().deleteRecord(values[0].getString(Constants.PARAM_TABLE_NAME),
                                             values[0].getLong(Constants.PARAM_ID));
        Intent deletedFromDbIntent = new Intent(Constants.ACTION_RESULT_DELETED_FROM_DB);
        context.sendBroadcast(deletedFromDbIntent);
    }

    @Override
    protected String doInBackground(Bundle... params) {
        ContentValues cv = params[0].getParcelable(Constants.PARAM_DATA);
        String uri = cv.getAsString(Constants.PARAM_URI);
        String tableName = cv.getAsString(Constants.PARAM_TABLE_NAME);
        long id = cv.getAsLong(Constants.PARAM_ID);

        String objectId = null;
        Cursor cursor = DBDebtors.getInstance().getRecord(tableName,id);
        if (cursor != null){
            if (cursor.moveToFirst()){
                objectId = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_OBJECT_ID));
            }
        }

        Bundle deleteParams = new Bundle();
        deleteParams.putLong(Constants.PARAM_ID, id);
        deleteParams.putString(Constants.PARAM_TABLE_NAME, tableName);
        publishProgress(deleteParams);

        if (Constants.DB_TABLE_DEBTORS.equals(tableName)) deleteAllDebts(objectId);

        if (objectId == null) return null;

        HttpClient client = new DefaultHttpClient();
        HttpDelete deleteRequest = new HttpDelete(uri + "/" + objectId);
        deleteRequest.setHeader(Constants.REST_API_KEY_NAME, Constants.REST_API_KEY);
        deleteRequest.setHeader(Constants.APPLICATION_ID_NAME, Constants.APPLICATION_ID);
        deleteRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");
        try {
            client.execute(deleteRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tableName;
    }

    @Override
    protected void onPostExecute(String tableName) {
        super.onPostExecute(tableName);
        String action = "";
        if (Constants.DB_TABLE_DEBTORS.equals(tableName)){
            action = Constants.ACTION_RESULT_DEBTOR_DELETED;
        }
        if (Constants.DB_TABLE_DEBTS.equals(tableName)){
            action = Constants.ACTION_RESULT_DEBT_DELETED;
        }
        Intent intent = new Intent(action);
        context.sendBroadcast(intent);
    }

    /**
     * Deletes all debts of the deleting debtor
     * @param objectId debtor's objectId
     */
    private void deleteAllDebts(String objectId){
        HttpClient client = new DefaultHttpClient();
        HttpPost deleteDebtsRequest = new HttpPost(Constants.URI_BATCH_DELETE);
        deleteDebtsRequest.setHeader(Constants.APPLICATION_ID_NAME, Constants.APPLICATION_ID);
        deleteDebtsRequest.setHeader(Constants.REST_API_KEY_NAME, Constants.REST_API_KEY);
        deleteDebtsRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");

        Cursor debtsCursor = DBDebtors.getInstance().getDataByField(
                    Constants.DB_TABLE_DEBTS, Constants.COLUMN_POINTER_DEBTOR, "'" + objectId + "'"); // get cursor with all debts of deleting debtor
        ArrayList<BatchDeletingObject> debts = new ArrayList<BatchDeletingObject>();
        Gson gson = new Gson();
        String pathDebt = "/1/classes/Debt/";
        if (debtsCursor != null){

                while (debtsCursor.moveToNext()){                                                    // create object to delete every debt
                    String debtObjectId = debtsCursor
                            .getString(debtsCursor.getColumnIndex(Constants.COLUMN_OBJECT_ID));      // get debts objectId
                    String path = pathDebt + debtObjectId;                                           // create path to delete debt from Parse.com
                    BatchDeletingObject obj = new BatchDeletingObject(Constants.METHOD_DELETE, path);// create special object representing single debt deleting in batch operation
                    debts.add(obj);
                    DBDebtors.getInstance().deleteRecord(
                            Constants.DB_TABLE_DEBTS, debtsCursor.getColumnIndex(Constants.COLUMN_ID));// delete record from database
                }
            debtsCursor.close();
        }
        try {
            HashMap<String, ArrayList<BatchDeletingObject>> map = new HashMap<String, ArrayList<BatchDeletingObject>>();// create hash map and put in debts list with key "requests" (Parse.com syntax)
            map.put("requests", debts);
            String json = gson.toJson(map);// parse map to json
            deleteDebtsRequest.setEntity(new StringEntity(json));
            client.execute(deleteDebtsRequest);// delete from Parse.com
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
