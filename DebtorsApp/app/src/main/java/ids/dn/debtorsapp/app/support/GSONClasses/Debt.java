package ids.dn.debtorsapp.app.support.GSONClasses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dev4 on 06.03.14.
 */
public class Debt {
    @SerializedName("amount")
    public int amount;
    @SerializedName("date_loan")
    public String date_loan;
    @SerializedName("date_return")
    public String date_return;
    @SerializedName("debtor")
    public Pointer debtor;
    @SerializedName("photo_on_parse")
    public String photoOnParse;
    @SerializedName("photo")
    public String photo;
    @SerializedName("is_debt_money")
    public int is_debt_money;
    @SerializedName("name")
    public String name;
    @SerializedName("remind_event_id")
    public long remind_event_id;

    public Debt(int amount, String date_loan, String date_return, Pointer debtor, String photoOnParse, String photo, int is_debt_money, String name,long remind_event_id) {
        this.amount = amount;
        this.date_loan = date_loan;
        this.date_return = date_return;
        this.debtor = debtor;
        this.photoOnParse = photoOnParse;
        this.photo = photo;
        this.is_debt_money = is_debt_money;
        this.name = name;
        this.remind_event_id = remind_event_id;
    }
}
