package ids.dn.debtorsapp.app.tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import ids.dn.debtorsapp.app.listeners.TaskListener;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 24.03.14.
 */
public class PostRequestAsyncTask extends AsyncTask<String, Void, String> {

    Context context;
    TaskListener listener;

    public PostRequestAsyncTask(Context context) {
        this.context = context;
        listener = (TaskListener) context;
    }

    @Override
    protected void onPreExecute(){

    }

    @Override
    protected String doInBackground(String... urls) {
        String email = null;
        if(urls.length>0){
            String url = urls[0];
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            try{
                HttpResponse response = httpClient.execute(httppost);
                if(response!=null){
                    //If status is OK 200
                    if(response.getStatusLine().getStatusCode()==200){
                        String result = EntityUtils.toString(response.getEntity());
                        //Convert the string result to a JSON Object
                        JSONObject resultJson = new JSONObject(result);
                        //Extract data from JSON Response
                        int expiresIn = resultJson.has("expires_in") ? resultJson.getInt("expires_in") : 0;

                        String accessToken = resultJson.has("access_token") ? resultJson.getString("access_token") : null;
                        Log.e("Tokenm", ""+accessToken);
                        if(expiresIn>0 && accessToken!=null){
                            Log.i("Authorize", "This is the access Token: "+accessToken+". It will expires in "+expiresIn+" secs");

                            //Calculate date of expiration
                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.SECOND, expiresIn);
                            long expireDate = calendar.getTimeInMillis();

                            //Store both expires in and access token in shared preferences
                            String request = "https://api.linkedin.com/v1/people/~:(email-address)?oauth2_access_token=" + accessToken + "&format=json";
                            HttpGet get = new HttpGet(request);
                            HttpResponse resp = httpClient.execute(get);
                            JSONObject json = Support.parseToJSONObject(resp.getEntity().getContent());
                            email = json.getString("emailAddress");
                        }
                    }
                }
            }catch(IOException e){
                Log.e("Authorize","Error Http response "+e.getLocalizedMessage());
            }
            catch (ParseException e) {
                Log.e("Authorize","Error Parsing Http response "+e.getLocalizedMessage());
            } catch (JSONException e) {
                Log.e("Authorize", "Error Parsing Http response " + e.getLocalizedMessage());
            }
        }
        return email;
    }

    @Override
    protected void onPostExecute(String email){
        if(!TextUtils.isEmpty(email)){
            listener.returnString(email);
        }
    }
}