package ids.dn.debtorsapp.app.tasks;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.listeners.TaskListener;
import ids.dn.debtorsapp.app.support.Constants;

/**
 * Created by dev4 on 28.02.14.
 */
public class GetRecordTask extends AsyncTask<Bundle, Void, Cursor> {

    TaskListener taskListener;

    public GetRecordTask(TaskListener taskListener) {
        this.taskListener = taskListener;
    }

    @Override
    protected Cursor doInBackground(Bundle... params) {
        String tableName = params[0].getString(Constants.PARAM_TABLE_NAME);
        long id = params[0].getLong(Constants.PARAM_ID);
        return DBDebtors.getInstance().getRecord(tableName,id);
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        super.onPostExecute(cursor);
        taskListener.fireTaskComplete(cursor);
    }
}
