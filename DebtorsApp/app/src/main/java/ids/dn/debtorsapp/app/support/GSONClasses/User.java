package ids.dn.debtorsapp.app.support.GSONClasses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dev4 on 12.03.14.
 */
public class User {

    @SerializedName("username")
    String name;
    @SerializedName("password")
    String password;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }
}
