package ids.dn.debtorsapp.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import ids.dn.debtorsapp.app.R;
import ids.dn.debtorsapp.app.activities.DebtorsListActivity;
import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.services.DebtorsHttpService;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 06.03.14.
 */
public class FragmentTabsAddDebt extends FragmentBase implements View.OnClickListener{

    public static final String TAB_MONEY = "Money";
    public static final String TAB_STUFF = "Stuff";
    public static final int REMIND_PERIOD_MINUTES = 3;

    private View mRoot;
    private TabHost mTabHost;
    //Money views
    EditText etAddDebtMoneyAmount;
    EditText etAddDebtMoneyDateLoan;
    EditText etAddDebtMoneyDateReturn;
    EditText etAddDebtMoneyRemind;
    CheckBox cbMoneyRemindTime;
    Button bAddDebtMoney;
    // Stuff views
    EditText etAddDebtStuffName;
    EditText etAddDebtStuffDateLoan;
    EditText etAddDebtStuffDateReturn;
    EditText etAddDebtStuffRemind;
    CheckBox cbStuffRemindTime;
    Button bAddDebtStuff;

    boolean isBound = false;
    String localPathToPhoto;
    String debtorsObjectId;
    String debtorsName;
    String debtorsSurname;
    ImageView ivStuffPhoto;

    boolean isEdit = false;
    boolean isPhotoChanged = false;
    boolean isRemind = false;
    boolean addEventMoneyDebt = false;
    boolean addEventStuffDebt = false;
    String currentTab;
    long addedEventId = -1;
    long remindersId;

    /**
     * fields for edit debt operation
     */
    long id;
    int amount;
    String name = "";
    String dateLoan = "";
    String dateReturn = "";
    String debtor;
    int isMoneyDebt = 1;
    String photo;
    String pathOnParse;
    String objectId;

    public void setParameters(String pathOnParse, long id, int amount, String name, String dateLoan,
                               String dateReturn, String debtor, int isMoneyDebt, String photo, String debtorsObjectId,String objectId, long remind_event_id) {
        this.pathOnParse = pathOnParse;
        this.id = id;
        this.amount = amount;
        this.name = name;
        this.dateLoan = dateLoan;
        this.dateReturn = dateReturn;
        this.debtor = debtor;
        this.isMoneyDebt = isMoneyDebt;
        this.photo = photo;
        this.debtorsObjectId = debtorsObjectId;
        this.objectId = objectId;
        this.addedEventId = remind_event_id;
        if (addedEventId != -1){
            if (isMoneyDebt == 1){
                addEventMoneyDebt = true;
            } else {
                addEventStuffDebt = true;
            }
        }
        /**
         * If objectId exists - then contact is editing
         */
        if (!"".equals(objectId)){
            isEdit = true;
        }
    }

    public void setDebtorsObjectId(String debtorsObjectId) {
        this.debtorsObjectId = debtorsObjectId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DBDebtors.getInstance().open();
        mRoot = inflater.inflate(R.layout.fragment_add_debt, null);
        mTabHost = (TabHost) mRoot.findViewById(android.R.id.tabhost);
        setupTabs();
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                currentTab = tabId;
            }
        });

        new GetDebtorTask().execute();
        // Add money layout
        etAddDebtMoneyAmount = (EditText) mRoot.findViewById(R.id.etAddDebtMoneyAmnt);
        etAddDebtMoneyDateLoan = (EditText) mRoot.findViewById(R.id.etAddDebtMoneyDateLoan);
        etAddDebtMoneyDateLoan.setOnClickListener(this);
        etAddDebtMoneyDateReturn = (EditText) mRoot.findViewById(R.id.etAddDebtMoneyDateReturn);
        etAddDebtMoneyDateReturn.setOnClickListener(this);
        bAddDebtMoney = (Button) mRoot.findViewById(R.id.bAddDebtMoney);
        bAddDebtMoney.setOnClickListener(this);
        cbMoneyRemindTime = (CheckBox) mRoot.findViewById(R.id.cbMoneyRemind);
        cbMoneyRemindTime.setOnClickListener(this);
        etAddDebtMoneyRemind = (EditText) mRoot.findViewById(R.id.etMoneyRemindTime);
        etAddDebtMoneyRemind.setOnClickListener(this);

        // Add stuff layout
        etAddDebtStuffDateLoan = (EditText) mRoot.findViewById(R.id.etAddDebtStuffDateLoan);
        etAddDebtStuffDateLoan.setOnClickListener(this);
        etAddDebtStuffName = (EditText) mRoot.findViewById(R.id.etAddDebtStuffName);
        etAddDebtStuffDateReturn = (EditText) mRoot.findViewById(R.id.etAddDebtStuffDateReturn);
        etAddDebtStuffDateReturn.setOnClickListener(this);
        ivStuffPhoto = (ImageView) mRoot.findViewById(R.id.ivStuffPhoto);
        ivStuffPhoto.setOnClickListener(this);
        bAddDebtStuff = (Button) mRoot.findViewById(R.id.bAddDebtStuff);
        bAddDebtStuff.setOnClickListener(this);
        cbStuffRemindTime = (CheckBox) mRoot.findViewById(R.id.cbStuffRemindTime);
        cbStuffRemindTime.setOnClickListener(this);
        etAddDebtStuffRemind = (EditText) mRoot.findViewById(R.id.etStuffRemindTime);
        etAddDebtStuffRemind.setOnClickListener(this);

        if (currentTab != null){
            if(currentTab.equals(TAB_MONEY)){
                mTabHost.setCurrentTab(0);
            } else {
                mTabHost.setCurrentTab(1);
                if (localPathToPhoto == null && photo != null){
                    localPathToPhoto = photo;
                }
                Bitmap bitmap = Support.getBitmap(localPathToPhoto,getActivity().getContentResolver(), false);
                ivStuffPhoto.setImageBitmap(bitmap);
                if(((BitmapDrawable)ivStuffPhoto.getDrawable()).getBitmap() == null){
                    ivStuffPhoto.setImageResource(R.drawable.no_image);
                }
            }
        }

        // Set current tab and fill data
        if (isEdit){
            ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_update_debt);
            switch (isMoneyDebt){
                case 0 :
                    bAddDebtStuff.setText(R.string.button_update_text);
                    mTabHost.setCurrentTab(1);
                    etAddDebtStuffName.setText(name);
                    etAddDebtStuffDateLoan.setText(dateLoan);
                    etAddDebtStuffDateReturn.setText(dateReturn);
                    if (photo != null){
                        localPathToPhoto = photo;
                        Bitmap bitmapPhoto = Support.getBitmap(localPathToPhoto,getActivity().getContentResolver(), false);
                        if (bitmapPhoto != null){
                            ivStuffPhoto.setImageBitmap(bitmapPhoto);
                        } else {
                            ivStuffPhoto.setImageResource(R.drawable.no_image);
                        }
                    }
                    if (addedEventId != -1){
                        isRemind = true;
                        cbStuffRemindTime.setChecked(true);
                        etAddDebtStuffRemind.setText(getEventRemindersTime());
                    }
                    break;
                case 1 :
                    bAddDebtMoney.setText(R.string.button_update_text);
                    // not necessary to set tab this, because it's default
                    mTabHost.setCurrentTab(0);
                    etAddDebtMoneyAmount.setText("" + amount);
                    etAddDebtMoneyDateLoan.setText(dateLoan);
                    etAddDebtMoneyDateReturn.setText(dateReturn);
                    if (addedEventId != -1){
                        isRemind = true;
                        cbMoneyRemindTime.setChecked(true);
                        etAddDebtMoneyRemind.setText(getEventRemindersTime());
                    }
                    break;
            }
        } else {
            ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_add_new_debt);
        }
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return mRoot;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DBDebtors.getInstance().close();
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().bindService(new Intent(getActivity(), DebtorsHttpService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!isBound) return;
        getActivity().unbindService(serviceConnection);
        isBound = false;
    }

    private void setupTabs() {
        mTabHost.setup(); // you must call this before adding your tabs!
        mTabHost.addTab(newTab(TAB_MONEY, R.id.tab_1));
        mTabHost.addTab(newTab(TAB_STUFF, R.id.tab_2));
    }

    private TabHost.TabSpec newTab(String tag,int tabContentId) {
        View indicator = LayoutInflater.from(getActivity()).inflate(
                R.layout.tab,
                (ViewGroup) mRoot.findViewById(android.R.id.tabs), false);
        ((TextView) indicator.findViewById(R.id.text)).setText(tag);
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag);
        tabSpec.setIndicator(indicator);
        tabSpec.setContent(tabContentId);
        return tabSpec;
    }

    @Override
    public void onClick(View v) {
        FragmentDatePicker fragmentDatePicker;
        FragmentTimePicker fragmentTimePicker;
        String tagDate = "datePicker";
        String tagTime = "timePicker";
        ContentValues cv = new ContentValues();
        switch (v.getId()){
            case R.id.bAddDebtMoney :
                if (!checkMoneyTabValidity()){
                    return;
                }
                if (addEventMoneyDebt) {
                    saveCalendarEvent(etAddDebtMoneyDateReturn.getText().toString(), etAddDebtMoneyRemind.getText().toString());
                }
                cv.put(Constants.COLUMN_AMOUNT, Integer.parseInt(etAddDebtMoneyAmount.getText().toString()));
                cv.put(Constants.COLUMN_DATE_LOAN, etAddDebtMoneyDateLoan.getText().toString());
                cv.put(Constants.COLUMN_DATE_RETURN, etAddDebtMoneyDateReturn.getText().toString());
                cv.put(Constants.COLUMN_POINTER_DEBTOR, debtorsObjectId);
                cv.put(Constants.COLUMN_REMIND_EVENT_ID, addedEventId);
                cv.put(Constants.COLUMN_IS_DEBT_MONEY, Constants.DEBT_TYPE_MONEY);
                if (!isEdit){
                    debtorsHttpService.uploadDebt(cv);
                } else {
                    if (!addEventMoneyDebt){
                        deleteEvent();
                    }
                    cv.put(Constants.COLUMN_REMIND_EVENT_ID, addedEventId);
                    cv.put(Constants.COLUMN_OBJECT_ID, objectId);
                    cv.put(Constants.COLUMN_ID, id);
                    debtorsHttpService.updateDebtMoney(cv);
                }
                getActivity().onBackPressed();
                break;
            case R.id.bAddDebtStuff :
                if (!checkStuffTabValidity()){
                    return;
                }
                if(addEventStuffDebt) {
                    saveCalendarEvent(etAddDebtStuffDateReturn.getText().toString(), etAddDebtStuffRemind.getText().toString());
                }
                cv.put(Constants.COLUMN_NAME,etAddDebtStuffName.getText().toString());
                cv.put(Constants.COLUMN_DATE_LOAN, etAddDebtStuffDateLoan.getText().toString());
                cv.put(Constants.COLUMN_DATE_RETURN, etAddDebtStuffDateReturn.getText().toString());
                cv.put(Constants.COLUMN_POINTER_DEBTOR, debtorsObjectId);
                cv.put(Constants.COLUMN_PHOTO,localPathToPhoto);
                cv.put(Constants.COLUMN_REMIND_EVENT_ID, addedEventId);
                cv.put(Constants.COLUMN_IS_DEBT_MONEY, Constants.DEBT_TYPE_STUFF);

                if (!isPhotoChanged && !isEdit){
                    debtorsHttpService.uploadDebt(cv);
                } else {
                    if (!addEventStuffDebt){
                        deleteEvent();
                    }
                    cv.put(Constants.COLUMN_REMIND_EVENT_ID, addedEventId);
                    cv.put(Constants.COLUMN_OBJECT_ID, objectId);
                    cv.put(Constants.COLUMN_PHOTO_PARSE_PATH, pathOnParse);
                    cv.put(Constants.COLUMN_ID, id);
                    debtorsHttpService.updateDebtStuff(cv);
                }
                getActivity().onBackPressed();
                break;
            case R.id.etAddDebtMoneyDateLoan :
                fragmentDatePicker = new FragmentDatePicker(etAddDebtMoneyDateLoan);
                fragmentDatePicker.show(getActivity().getSupportFragmentManager(),tagDate);
                break;
            case R.id.etAddDebtMoneyDateReturn :
                fragmentDatePicker = new FragmentDatePicker(etAddDebtMoneyDateReturn);
                fragmentDatePicker.show(getActivity().getSupportFragmentManager(),tagDate);
                break;
            case R.id.etAddDebtStuffDateLoan :
                fragmentDatePicker = new FragmentDatePicker(etAddDebtStuffDateLoan);
                fragmentDatePicker.show(getActivity().getSupportFragmentManager(),tagDate);
                break;
            case R.id.etAddDebtStuffDateReturn :
                fragmentDatePicker = new FragmentDatePicker(etAddDebtStuffDateReturn);
                fragmentDatePicker.show(getActivity().getSupportFragmentManager(),tagDate);
                break;
            case R.id.ivStuffPhoto:
                openPhotoDialog();
                break;
            case R.id.cbMoneyRemind :
                if (checkEventValidity(etAddDebtMoneyDateReturn, etAddDebtMoneyRemind, cbMoneyRemindTime)){
                    if (!addEventMoneyDebt){
                        addEventMoneyDebt = true;
                    } else {
                        addEventMoneyDebt = false;
                    }
                }
                break;
            case R.id.cbStuffRemindTime :
                if (checkEventValidity(etAddDebtStuffDateReturn, etAddDebtStuffRemind, cbStuffRemindTime)){
                    if (!addEventStuffDebt){
                        addEventStuffDebt = true;
                    } else {
                        addEventStuffDebt = false;
                    }
                }
                break;
            case R.id.etMoneyRemindTime :
                fragmentTimePicker = new FragmentTimePicker(etAddDebtMoneyRemind);
                fragmentTimePicker.show(getActivity().getSupportFragmentManager(), tagTime);
                break;
            case R.id.etStuffRemindTime :
                fragmentTimePicker = new FragmentTimePicker(etAddDebtStuffRemind);
                fragmentTimePicker.show(getActivity().getSupportFragmentManager(), tagTime);
                break;
        }
    }

    private boolean checkEventValidity(EditText etDate, EditText etTime, CheckBox cb){
        if (TextUtils.isEmpty(etDate.getText().toString())){
            cb.setChecked(false);
            Toast.makeText(getActivity(), "Enter date return ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(etTime.getText().toString())) {
            cb.setChecked(false);
            Toast.makeText(getActivity(), "Enter time to remind", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            switch (requestCode){
               case Constants.REQUEST_CAMERA_PIC:
                    // DO NOTHING
                    break;
               case Constants.REQUEST_TAKE_PHOTO_FROM_SD:
                   localPathToPhoto = Support.getRealPathFromURI(data.getData(), getActivity().getContentResolver());
                   break;
            }
            ivStuffPhoto.setImageBitmap(Support.getBitmap(localPathToPhoto, getActivity().getContentResolver(), false));
            /**
             * Checking if current debt editing and we are here - it means that we changed photo of
             * stuff debt, so we making path to this photo on Parse.com empty and changing flag value
             * isPhotoChanged to true
             */
            if (isEdit){
                isPhotoChanged = true;
                pathOnParse = "";
            }
        }
    }

    private boolean checkMoneyTabValidity(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyy");
        if (TextUtils.isEmpty(etAddDebtMoneyAmount.getText().toString())){
            Toast.makeText(getActivity(), R.string.error_empty_amount ,Toast.LENGTH_SHORT).show();
            return false;
        }
        try {
            long dateLoan = formatter.parse(etAddDebtMoneyDateLoan.getText().toString()).getTime();
            long dateReturn = formatter.parse(etAddDebtMoneyDateReturn.getText().toString()).getTime();
            if (dateLoan > dateReturn){
                Toast.makeText(getActivity(), R.string.error_dates,Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (ParseException e) {
            Toast.makeText(getActivity(), R.string.error_empty_dates,Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkStuffTabValidity(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyy");
        if (TextUtils.isEmpty(etAddDebtStuffName.getText().toString())){
            Toast.makeText(getActivity(), R.string.error_stuff_name ,Toast.LENGTH_SHORT).show();
            return false;
        }
        try {
            long dateLoan = formatter.parse(etAddDebtStuffDateLoan.getText().toString()).getTime();
            long dateReturn = formatter.parse(etAddDebtStuffDateReturn.getText().toString()).getTime();
            if (dateLoan > dateReturn){
                Toast.makeText(getActivity(), R.string.error_dates ,Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (ParseException e) {
            Toast.makeText(getActivity(), R.string.error_empty_dates ,Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void openPhotoDialog(){
        AlertDialog.Builder newDebtorDialog = new AlertDialog.Builder(getActivity());
        newDebtorDialog.setTitle("Choose operation");
        newDebtorDialog.setPositiveButton(R.string.photo_from_sd_card, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, Constants.REQUEST_TAKE_PHOTO_FROM_SD);
            }
        });
        newDebtorDialog.setNegativeButton(R.string.photo_from_camera, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String photoName = "Debt" + timeStamp;
                File photo = new File(DebtorsListActivity.photoDirectoryPath + File.separator + photoName + ".jpg");
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                localPathToPhoto= photo.getPath();
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                startActivityForResult(cameraIntent, Constants.REQUEST_CAMERA_PIC);
            }
        });
        newDebtorDialog.show();
    }


    private void saveCalendarEvent(String date, String time){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH:mm");
        sdf.setTimeZone(TimeZone.getDefault());
        Date dateRemind = null;
        try {
            dateRemind = sdf.parse(date + "." + time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar startDate = Calendar.getInstance();
        startDate.setTime(dateRemind);
        Calendar endDate = Calendar.getInstance();
        endDate.setTime(dateRemind);

        String title = debtorsName + " " + debtorsSurname + " owe you something. Check out.";
        ContentValues calEvent = new ContentValues();
        calEvent.put(CalendarContract.Events.CALENDAR_ID, 1);   // Default calendar
        calEvent.put(CalendarContract.Events.TITLE, title);
        // to prevent earlier remind adding 1 minute
        calEvent.put(CalendarContract.Events.DTSTART, startDate.getTimeInMillis() + 1000 * 60 * 1);
        calEvent.put(CalendarContract.Events.DTEND, endDate.getTimeInMillis() + 1000 * 60 * REMIND_PERIOD_MINUTES);
        calEvent.put(CalendarContract.Events.HAS_ALARM, "1");
        calEvent.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
        Uri uri = getActivity().getContentResolver().insert(Uri.parse("content://com.android.calendar/events"), calEvent);
        addedEventId = Long.parseLong(uri.getLastPathSegment());

        // reminder insert
        Uri REMINDERS_URI = Uri.parse("content://com.android.calendar/reminders");
        calEvent = new ContentValues();
        calEvent.put(CalendarContract.Reminders.EVENT_ID, addedEventId);
        calEvent.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        calEvent.put(CalendarContract.Reminders.MINUTES, 1);
        Uri remindersUri = getActivity().getContentResolver().insert(REMINDERS_URI, calEvent);
        remindersId = Long.parseLong(remindersUri.getLastPathSegment());
    }

    private void deleteEvent(){
        if (addedEventId != -1) {
            getActivity().getContentResolver().
                    delete(Uri.parse("content://com.android.calendar/events" + File.separator + addedEventId),
                            null, null);
            addedEventId = -1;
            getActivity().getContentResolver().
                    delete(Uri.parse("content://com.android.calendar/reminders" + File.separator + remindersId),
                            null, null);
            remindersId = -1;
        }
    }

    private String getEventRemindersTime(){
        String time = null;
        String[] PROJECTION = {CalendarContract.Events.DTSTART};
        String SELECTION = CalendarContract.Events._ID + " = " + addedEventId;
        Cursor eventCursor = getActivity().getContentResolver()
                .query((Uri.parse("content://com.android.calendar/events")),PROJECTION,SELECTION,null,null);
        if (eventCursor != null){
            if (eventCursor.moveToFirst()){
                long eventTime = eventCursor.getLong(eventCursor.getColumnIndex(CalendarContract.Events.DTSTART));
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                Date date = new Date(eventTime);
                time = sdf.format(date).toString();
            }
            eventCursor.close();
        }
        return time;
    }

    class GetDebtorTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            Cursor cursor = DBDebtors.getInstance().getDataByField(Constants.DB_TABLE_DEBTORS, Constants.COLUMN_OBJECT_ID, "'" + debtorsObjectId + "'");
            if (cursor != null){
                if (cursor.moveToFirst()){
                    debtorsName = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_NAME));
                    debtorsSurname = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_SURNAME));
                }
                cursor.close();
            }
            return null;
        }
    }
}
