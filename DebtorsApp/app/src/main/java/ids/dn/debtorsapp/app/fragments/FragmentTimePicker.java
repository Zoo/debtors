package ids.dn.debtorsapp.app.fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ids.dn.debtorsapp.app.support.Support;

/**
 * Created by dev4 on 18.03.14.
 */
public class FragmentTimePicker extends DialogFragment {

    EditText editText;

    public FragmentTimePicker(EditText editText) {
        this.editText = editText;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        if (!TextUtils.isEmpty(editText.getText().toString())){
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                Date date = sdf.parse(editText.getText().toString());
                c.setTime(date);
            } catch (ParseException e) {
                Log.d("<-- PARSING ERROR -->", "Error while parsing time");
            }
        }
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(),myCallback, hour, minute, true);
    }


    TimePickerDialog.OnTimeSetListener myCallback = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar c = Calendar.getInstance();
            c.set(c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            editText.setText(sdf.format(c.getTime()));
        }
    };
}
