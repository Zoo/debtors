package ids.dn.debtorsapp.app.fragments;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;

import ids.dn.debtorsapp.app.listeners.FragmentListener;
import ids.dn.debtorsapp.app.services.DebtorsHttpService;
import ids.dn.debtorsapp.app.support.Constants;

/**
 * Created by dev4 on 20.03.14.
 */
public class FragmentBase extends Fragment {

    protected DebtorsHttpService debtorsHttpService;
    protected ServiceConnection serviceConnection;
    protected boolean isBound = false;
    FragmentListener fragmentListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                debtorsHttpService = ((DebtorsHttpService.ServiceBinder)binder).getService();
                isBound = true;
                Intent serviceConnectedIntent = new Intent(Constants.ACTION_RESULT_SERVICE_CONNECTED);
                getActivity().sendBroadcast(serviceConnectedIntent);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                isBound = false;
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().bindService(new Intent(getActivity(), DebtorsHttpService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!isBound) return;
        getActivity().unbindService(serviceConnection);
        isBound = false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentListener = (FragmentListener) activity;
    }
}
