package ids.dn.debtorsapp.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ids.dn.debtorsapp.app.support.Constants;

/**
 * Created by dev4 on 28.02.14.
 */
public class DBDebtors {

    private static DBDebtors INSTANCE;
    SQLiteDatabase db;
    DBHelper dbHelper;

    public static synchronized DBDebtors getInstance(){
        if (INSTANCE == null){
            INSTANCE = new DBDebtors();
        }
        return INSTANCE;
    }

    public void create(Context ctx){
        dbHelper = new DBHelper(ctx, Constants.DB_NAME);
    }

    public void open(){
        db = dbHelper.getWritableDatabase();
    }

    public void close(){
        if (dbHelper != null){
            dbHelper.close();
        }
    }

    public Cursor getAllDataFromTable(String tableName){
        return db.query(tableName, null, null, null, null, null, null);
    }

    public void deleteRecord(String table_name, long id){
       db.delete(table_name, "_id = " + id, null);
    }

    public long addRecord(String table_name, ContentValues contentValues){
        return db.insert(table_name, null, contentValues);
    }

    public void updateRecord(String table_name, long id, ContentValues contentValues){
        db.update(table_name, contentValues, "_id = " + id, null);
    }

    public Cursor getDataByRawQuery(String query){
        return db.rawQuery(query,null);
    }

    public Cursor getRecord(String tableName,long id){
        return db.query(tableName, null, "_id = " + id, null, null , null, null);
    }

    public Cursor getDataByField(String tableName, String fieldName, String value){
        return db.query(tableName, null, fieldName + " = " + value, null, null , null, null);
    }

    class DBHelper extends SQLiteOpenHelper {

        DBHelper(Context context, String name) {
            super(context, name, null, Constants.DB_VERSION);
        }

        @Override
        public void onOpen(SQLiteDatabase db) {
            super.onOpen(db);
            db.execSQL("PRAGMA foreign_keys=ON;");
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Constants.DB_TABLE_DEBTORS + "(" + Constants.COLUMN_ID + " integer primary key autoincrement, " +
                                                                            Constants.COLUMN_OBJECT_ID + " text, " +
                                                                            Constants.COLUMN_NAME + " text, " +
                                                                            Constants.COLUMN_SURNAME + " text, " +
                                                                            Constants.COLUMN_TELEPHONE_NUMBER + " text, " +
                                                                            Constants.COLUMN_EMAIL + " text, " +
                                                                            Constants.COLUMN_PHOTO + " text, " +
                                                                            Constants.COLUMN_USER + " text, " +
                                                                            Constants.COLUMN_PHOTO_PARSE_PATH + " text);");
            db.execSQL("CREATE TABLE " + Constants.DB_TABLE_DEBTS + "(" + Constants.COLUMN_ID + " integer primary key autoincrement, " +
                                                                            Constants.COLUMN_OBJECT_ID + " text, " +
                                                                            Constants.COLUMN_DATE_LOAN + " text, " +
                                                                            Constants.COLUMN_DATE_RETURN + " text, " +
                                                                            Constants.COLUMN_REMIND_EVENT_ID + " text, " +
                                                                            Constants.COLUMN_NAME + " text, " +
                                                                            Constants.COLUMN_AMOUNT + " integer, " +
                                                                            Constants.COLUMN_PHOTO_PARSE_PATH + " text, " +
                                                                            Constants.COLUMN_PHOTO + " text, " +
                                                                            Constants.COLUMN_IS_DEBT_MONEY + " integer, " +
                                                                            Constants.COLUMN_POINTER_DEBTOR + " text);");
            db.execSQL("CREATE TABLE " + Constants.DB_TABLE_USERS + "(" + Constants.COLUMN_ID + " integer primary key autoincrement, " +
                                                                            Constants.COLUMN_NAME + " text, " +
                                                                            Constants.COLUMN_OBJECT_ID + " text);");
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
