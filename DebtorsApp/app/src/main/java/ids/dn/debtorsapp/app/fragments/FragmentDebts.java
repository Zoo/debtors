package ids.dn.debtorsapp.app.fragments;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.CursorAdapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import ids.dn.debtorsapp.app.R;
import ids.dn.debtorsapp.app.database.DBDebtors;
import ids.dn.debtorsapp.app.database.DBDebtsLoader;
import ids.dn.debtorsapp.app.listeners.TaskListener;
import ids.dn.debtorsapp.app.support.Constants;
import ids.dn.debtorsapp.app.support.Support;
import ids.dn.debtorsapp.app.tasks.GetRecordTask;

/**
 * Created by dev4 on 06.03.14.
 */
public class FragmentDebts extends FragmentBase implements LoaderManager.LoaderCallbacks<Cursor>, TaskListener{

    String debtorsObjectId;
    BroadcastReceiver broadcastReceiver;
    ListView listView;

    public void setDebtorsObjectId(String debtorsObjectId) {
        this.debtorsObjectId = debtorsObjectId;
    }

    ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.ic_launcher)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .cacheOnDisc(true)
            .cacheInMemory(true)
            .showImageOnLoading(R.drawable.load)
            .showImageForEmptyUri(R.drawable.no_photo)
            .build();

    DebtsCursorAdapter debtsCursorAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        DBDebtors.getInstance().open();
        View v = inflater.inflate(R.layout.fragment_list, null);
        getLoaderManager().initLoader(Constants.LOADER_ID_DEBTS, null, this);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Constants.ACTION_RESULT_PHOTO_UPLOADED)){
                    Toast.makeText(getActivity(), "Debt uploaded", Toast.LENGTH_SHORT).show();
                }
                if (intent.getAction().equals(Constants.ACTION_RESULT_DEBT_DELETED)){
                    Toast.makeText(getActivity(), "Debt deleted", Toast.LENGTH_SHORT).show();
                }
                if (intent.getAction().equals(Constants.ACTION_RESULT_DEBT_UPDATED)){
                    Toast.makeText(getActivity(), "Debt updated", Toast.LENGTH_SHORT).show();
                }
                getLoaderManager().getLoader(Constants.LOADER_ID_DEBTS).forceLoad();
            }
        };

        listView = (ListView) v.findViewById(R.id.listView);
        debtsCursorAdapter = new DebtsCursorAdapter(getActivity(), null,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER, inflater);
        listView.setAdapter(debtsCursorAdapter);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DBDebtors.getInstance().close();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        registerForContextMenu(listView);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_debts);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_RESULT_PHOTO_UPLOADED);
        intentFilter.addAction(Constants.ACTION_RESULT_DEBT_DELETED);
        intentFilter.addAction(Constants.ACTION_RESULT_DEBT_UPDATED);
        intentFilter.addAction(Constants.ACTION_RESULT_DEBT_ADDED);
        intentFilter.addAction(Constants.ACTION_RESULT_DELETED_FROM_DB);
        intentFilter.addAction(Constants.ACTION_RESULT_DB_UPDATED);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new DBDebtsLoader(getActivity(), debtorsObjectId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        debtsCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        debtsCursorAdapter.swapCursor(null);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, Constants.ITEM_DELETE, 0, "Delete");
        menu.add(0, Constants.ITEM_EDIT, 0, "Edit");
        menu.add(0, Constants.ITEM_BACK, 0, "Back");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo;
        ContentValues cv;
        switch (item.getItemId()){
            case Constants.ITEM_DELETE :
                adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

                cv = new ContentValues();
                cv.put(Constants.PARAM_URI, Constants.URI_CLASS_DEBT);
                cv.put(Constants.PARAM_TABLE_NAME, Constants.DB_TABLE_DEBTS);
                cv.put(Constants.PARAM_ID, adapterContextMenuInfo.id);
                debtorsHttpService.delete(cv);
                break;
            case Constants.ITEM_EDIT :
                adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.PARAM_TABLE_NAME, Constants.DB_TABLE_DEBTS);
                bundle.putLong(Constants.PARAM_ID, adapterContextMenuInfo.id);
                new GetRecordTask(this).execute(bundle);
                break;
            case Constants.ITEM_BACK :
                getActivity().closeContextMenu();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_add_debt_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_add_debt :
                FragmentTabsAddDebt fragmentTabsAddDebt = new FragmentTabsAddDebt();
                fragmentTabsAddDebt.setDebtorsObjectId(debtorsObjectId);
                fragmentListener.replaceFragment(fragmentTabsAddDebt);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //=================================================
    //              Editing existing debt
    //=================================================
    @Override
    public void fireTaskComplete(Cursor cursor) {
        if (cursor != null){
            if (cursor.moveToFirst()){
                int isMoneyDebt = cursor.getInt(cursor.getColumnIndex(Constants.COLUMN_IS_DEBT_MONEY));
                long id = cursor.getLong(cursor.getColumnIndex(Constants.COLUMN_ID));
                String name = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_NAME));
                String dateLoan = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_DATE_LOAN));
                String dateReturn = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_DATE_RETURN));
                String debtor = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_POINTER_DEBTOR));
                String objectId = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_OBJECT_ID));
                Long remind_event_id = cursor.getLong(cursor.getColumnIndex(Constants.COLUMN_REMIND_EVENT_ID));
                FragmentTabsAddDebt fragmentTabsAddDebt;
                switch (isMoneyDebt){
                    case 0 :
                        String photo = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_PHOTO));
                        String photoOnParse = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_PHOTO_PARSE_PATH));
                        fragmentTabsAddDebt =new FragmentTabsAddDebt();
                        fragmentTabsAddDebt.setParameters(
                                photoOnParse,id,0,name,dateLoan,dateReturn,debtor,isMoneyDebt,photo,debtorsObjectId,objectId, remind_event_id);
                        fragmentListener.replaceFragment(fragmentTabsAddDebt);
                        break;
                    case 1 :
                        int amount = cursor.getInt(cursor.getColumnIndex(Constants.COLUMN_AMOUNT));
                        fragmentTabsAddDebt = new FragmentTabsAddDebt();
                        fragmentTabsAddDebt.setParameters(
                                "",id,amount,"",dateLoan,dateReturn,debtor,isMoneyDebt,"",debtorsObjectId,objectId, remind_event_id);
                        fragmentListener.replaceFragment(fragmentTabsAddDebt);
                        break;
                }
            }
            cursor.close();
        }
    }

    @Override
    public void returnString(String objectId) {

    }

    class DebtsCursorAdapter extends CursorAdapter {

        LayoutInflater layoutInflater;
        int debtType;

        DebtsCursorAdapter(Context context, Cursor c, int flags, LayoutInflater layoutInflater) {
            super(context, c, flags);
            this.layoutInflater = layoutInflater;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = null;
            switch (debtType){
                case Constants.DEBT_TYPE_MONEY :
                    view = layoutInflater.inflate(R.layout.item_debt_money, null);
                    break;
                case Constants.DEBT_TYPE_STUFF :
                    view = layoutInflater.inflate(R.layout.item_debt_stuff, null);
                    break;
            }
            bindView(view, context, cursor);
            return view;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            Cursor cursor = (Cursor) getItem(position);
            debtType = cursor.getInt(cursor.getColumnIndex(Constants.COLUMN_IS_DEBT_MONEY));
            return debtType;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            if (cursor != null){
                switch (debtType){
                    case Constants.DEBT_TYPE_MONEY :
                        TextView tvMoneyDateLoan = (TextView) view.findViewById(R.id.tvMoneyDateLoan);
                        TextView tvMoneyDateReturn = (TextView) view.findViewById(R.id.tvMoneyDateReturn);
                        TextView tvMoneyAmount = (TextView) view.findViewById(R.id.tvMoneyAmount);
                        ImageView ivMoney = (ImageView) view.findViewById(R.id.ivMoney);

                        tvMoneyDateLoan.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_DATE_LOAN)));
                        tvMoneyDateReturn.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_DATE_RETURN)));
                        ivMoney.setImageResource(R.drawable.itme_money);
                        tvMoneyAmount.setText("" + cursor.getInt(cursor.getColumnIndex(Constants.COLUMN_AMOUNT)));
                        break;
                    case Constants.DEBT_TYPE_STUFF :
                        TextView tvStuffDateLoan = (TextView) view.findViewById(R.id.tvStuffDateLoan);
                        TextView tvStuffDateReturn = (TextView) view.findViewById(R.id.tvStuffDateReturn);
                        TextView tvStuffName = (TextView) view.findViewById(R.id.tvStuffName);
                        ImageView ivStuffPhoto = (ImageView) view.findViewById(R.id.ivStuffPhoto);

                        tvStuffDateLoan.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_DATE_LOAN)));
                        tvStuffDateReturn.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_DATE_RETURN)));
                        tvStuffName.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_NAME)));
                        String pathOnParse = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_PHOTO_PARSE_PATH));
                        if (TextUtils.isEmpty(pathOnParse)){
                            String localPath = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_PHOTO));
                            Bitmap bitmap = Support.getBitmap(localPath, getActivity().getContentResolver(), true);
                            if (bitmap == null){
                                ivStuffPhoto.setImageResource(R.drawable.no_photo);
                            } else {
                                ivStuffPhoto.setImageBitmap(bitmap);
                            }
                        } else {
                            imageLoader.displayImage(pathOnParse,ivStuffPhoto, options);
                        }
                        break;
                }
            }
        }

    }
}
