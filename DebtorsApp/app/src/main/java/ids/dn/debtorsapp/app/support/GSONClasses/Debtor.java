package ids.dn.debtorsapp.app.support.GSONClasses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dev4 on 05.03.14.
 */
public class Debtor {

    public Debtor(String name, String surname, String phoneNumber, String email, String photoOnParse, String userObjectID) {
        this.name = name;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoOnParse = photoOnParse;
        this.userObjectID = userObjectID;
    }

    @SerializedName("name")
    public String name;
    @SerializedName("surname")
    public String surname;
    @SerializedName("telephone_number")
    public String phoneNumber;
    @SerializedName("email")
    public String email;
    @SerializedName("photo")
    public String photoOnParse;
    @SerializedName("user")
    public String userObjectID;
}
